-- 用户扩展信息
drop table if EXISTS `yoshop_user_ext`;
CREATE TABLE `yoshop_user_ext` (
`id`            int(11)       not null AUTO_INCREMENT,
`user_id`       int(11) unsigned       default 0  comment '用户id',
`store_id`      int(11) unsigned       default 0  comment '商城ID',

`client_ip`     varchar(20)   default '' comment  '用户客户端ip地址',
`idCard`        varchar(20)   default '' comment  '身份证号码',
`realName`      varchar(10)   default '' comment  '真实姓名',
`authenticate`  text  comment  '认证信息',
`authens`       tinyint(1) unsigned   default 0  comment '实名认证状态,1已认证 0未认证',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `user_id`   (`user_id`),
KEY  `store_id`   (`store_id`),
KEY  `authens`   (`authens`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户扩展信息' AUTO_INCREMENT=1 ;

-- 酒类业绩计算表
drop table if EXISTS `yoshop_perform`;
CREATE TABLE `yoshop_perform` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned      default 0 comment '商城ID',
`sort`          int(11) unsigned      DEFAULT '0'   COMMENT '排序号码',

`award`         tinyint(2) unsigned   DEFAULT 0    COMMENT '奖励百分比，15即15%',
`performance`   bigint(200) unsigned      default 0  comment '业绩金额',
`des`           varchar(100) default '' comment '描述',
`status`        tinyint(1)   default 1 comment '状态1正常0禁用',


`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned     DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)

) ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='酒类业绩计算表' AUTO_INCREMENT=1 ;

-- 用户业绩日结算表
drop table if EXISTS `yoshop_performance`;
CREATE TABLE `yoshop_performance` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned      default 0  comment '商城ID',
`user_id`       int(11) unsigned       default 0  comment '用户id',

`sales`         int(20) unsigned      default 0  comment '销量，单位件',
`retail`        decimal(11,2) unsigned  default 0  comment '达标奖，单位元',
`profit`        decimal(11,2) unsigned  default 0  comment '利润，单位元',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
key `user_id`  (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户业绩日结算表' AUTO_INCREMENT=1 ;

-- 经销商业绩计算表
drop table if EXISTS `yoshop_Dealer`;
CREATE TABLE `yoshop_Dealer` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned       default 0  comment '商城ID',

`per_wine`  decimal(11,2) unsigned default 0  comment '平级奖励 酒体，资金仅用于进货，单位元',
`dealers`       int(11) unsigned       default 0  comment '车奖30万条件,满足多少名C类经销商',
`car_award`     decimal(11,2) unsigned default 0  comment '车奖金额',
`car_wine`      decimal(11,2) unsigned default 0  comment '车奖酒体，资金仅用于进货，单位元',
`before`        int(11) unsigned       default 0  comment '市场分红条件 前几位',
`date_end`      date    default null  comment '市场分红条件 活动截止日期',
`total`         decimal(11,2) unsigned default 0  comment '市场分红条件 总业绩',
`total_q`       decimal(11,2) unsigned default 0  comment '市场分红条件 每季度业绩',
`profit_p`      int(2) unsigned        default 0  comment '市场分红条件 每季度分红率',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商业绩计算表' AUTO_INCREMENT=1 ;

-- 经销商成员表
drop table if EXISTS `yoshop_Dermember`;
CREATE TABLE `yoshop_Dermember` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11) unsigned       default 0  comment '商城ID',
`pid`           int(11) unsigned       default 0  comment '推荐者经销商id',
`user_id`       int(11) unsigned       default 0  comment '用户id',

`idCard`        varchar(20)   default '' comment '经销商身份证号码',
`total_expend_money`  decimal(11,2)  unsigned   default 0 comment '团队销售额',
`pcount`        int(11)  unsigned       default 0  comment '团队成员个数',
`gold`          decimal(11,2)  unsigned       default 0  comment '金币总额',
`gold_cash`     decimal(11,2)  unsigned       default 0  comment '金币提取额',
`gold_balance`  decimal(11,2)  unsigned       default 0  comment '金币余额',

`is_delete`     tinyint(3) unsigned   default 0  comment '是否删除',
`create_time`   int(11) unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11) unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
key `user_id`  (`user_id`),
key `pid`  (`pid`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商成员表' AUTO_INCREMENT=1 ;

-- 经销商下线记录表
drop table if EXISTS `yoshop_Derrecord`;
CREATE TABLE `yoshop_Derrecord` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户会员id',

`level`  tinyint(1)   unsigned    default 1   COMMENT '用户会员级别',
`mid`    int(50)      unsigned    default 0   COMMENT '下线用户会员id',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商下线记录表' AUTO_INCREMENT=1 ;


-- 经销商推荐金币记录表
drop table if EXISTS `yoshop_degain`;
CREATE TABLE `yoshop_degain` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`gain_type`     tinyint(1)    unsigned       default 0  COMMENT '收益类型1推荐奖励,2分享奖励,3活动报名推荐奖励,4活动报名出组奖励,5活动报名交通补贴',
`earnings`      decimal(11,2) default 0  comment '金币个数',
`sid`           int(50)       unsigned       default 0  COMMENT '用户(被推荐者)id',
`status`        tinyint(1)    unsigned      default 0     COMMENT '是否已经分金币 0未分配1已分配',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商推荐金币记录表' AUTO_INCREMENT=1 ;

-- 经销商金币提现记录表
drop table if EXISTS `yoshop_degain_cash`;
CREATE TABLE `yoshop_degain_cash` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`cash_type`     tinyint(1)    unsigned       default 0  COMMENT '支取平台类型1微信2支付宝3银行卡',
`earnings`      decimal(11,2) default 0  comment '金币个数',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商金币提现记录表' AUTO_INCREMENT=1 ;


-- 经销商业绩日结算表
drop table if EXISTS `yoshop_Dermance`;
CREATE TABLE `yoshop_Dermance` (
`id`            int(11)       not null AUTO_INCREMENT,
`store_id`      int(11)        unsigned  default 0  comment '商城ID',
`der_id`        int(11)        unsigned  default 0  comment '经销商id',
`sort`          int(11)        unsigned  default 0   comment '当前名次',
`balance_q`     decimal(11,2)  unsigned  default 0  comment '当前季度业绩',
`balance_all`   decimal(11,2)  unsigned  default 0  comment '总业绩',
`award_q`       decimal(11,2)  unsigned  default 0  comment '每季度应分红金额',


`is_delete`     tinyint(3) unsigned  default 0  comment '是否删除',
`create_time`   int(11)    unsigned  DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned  DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY   (`id`),
KEY  `der_id`   (`der_id`),
KEY  `store_id`   (`store_id`)

)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='经销商成员表' AUTO_INCREMENT=1 ;


-- 会员级别申请
drop table if EXISTS `yoshop_apply_grade`;
CREATE TABLE `yoshop_apply_grade` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`grade_id`      int(11)       unsigned       default 0  COMMENT '收益类型1推荐奖励,2分享奖励',
`status`        tinyint(1)    default 0      comment '0未审核1已审核',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`),
KEY  `grade_id`   (`grade_id`),
UNIQUE KEY  `grade_user_id`   (`user_id`,`grade_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='会员级别申请' AUTO_INCREMENT=1 ;


-- 个人信息采集
drop table if EXISTS `yoshop_member`;
CREATE TABLE `yoshop_member` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`name`          varchar(20)   default ''     COMMENT '姓名',
`mobile`        varchar(20)   default ''     COMMENT '手机号',
`idCard`        varchar(20)   default ''     COMMENT '身份证号',
`birthday`      varchar(20)   default ''     COMMENT '生日格式0813',
`sex`           tinyint(1)    unsigned default 1     COMMENT '性别1男2女',
`age`           tinyint(2)    unsigned default 1     COMMENT '年龄',
`height`        float(7,2)    unsigned default 0     COMMENT '身高cm',
`weight`        float(7,2)    unsigned default 0     COMMENT '体重kg',
`province`      varchar(20)   not null       COMMENT '省份',
`city`          varchar(20)   not null       COMMENT '城市',
`area`          varchar(20)   not null       COMMENT '城区',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
UNIQUE key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='个人信息采集' AUTO_INCREMENT=1 ;

-- 健康人群信息
drop table if EXISTS `yoshop_customer`;
CREATE TABLE `yoshop_customer` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`name`          varchar(20)   default ''     COMMENT '姓名',
`mobile`        varchar(20)   default ''     COMMENT '手机号',
`idCard`        varchar(20)   default ''     COMMENT '身份证号',
`birthday`      varchar(20)   default ''     COMMENT '生日格式0813',
`sex`           tinyint(1)    unsigned default 1     COMMENT '性别1男2女',
`age`           tinyint(2)    unsigned default 1     COMMENT '年龄',
`height`        float(7,2)    unsigned default 0     COMMENT '身高cm',
`weight`        float(7,2)    unsigned default 0     COMMENT '体重kg',
`bmi`           float(7,2)    unsigned default 0     COMMENT '体重指数',
`province`      varchar(20)   not null       COMMENT '省份',
`city`          varchar(20)   not null       COMMENT '城市',
`area`          varchar(20)   not null       COMMENT '城区',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='健康人群信息' AUTO_INCREMENT=1 ;


-- 患病人群信息
drop table if EXISTS `yoshop_customer_patient`;
CREATE TABLE `yoshop_customer_patient` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`name`          varchar(20)   default ''     COMMENT '姓名',
`mobile`        varchar(20)   default ''     COMMENT '手机号',
`idCard`        varchar(20)   default ''     COMMENT '身份证号',
`birthday`      varchar(20)   default ''     COMMENT '生日格式0813',
`sex`           tinyint(1)    unsigned default 1     COMMENT '性别1男2女',
`age`           tinyint(2)    unsigned default 1     COMMENT '年龄',
`province`      varchar(20)   not null       COMMENT '省份',
`city`          varchar(20)   not null       COMMENT '城市',
`area`          varchar(20)   not null       COMMENT '城区',
`type_illness`  varchar(20)   default ''             COMMENT '疾病类型 归属于 例如“健康志愿者”，“骨质疏松患者”等的id',
`case`          text    COMMENT '病例报告图片url',
`remark`        text    COMMENT '备注',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='患病人群信息' AUTO_INCREMENT=1 ;


-- 招募信息
drop table if EXISTS `yoshop_recruit`;
CREATE TABLE `yoshop_recruit` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`name`          varchar(20)   default ''     COMMENT '姓名',
`mobile`        varchar(20)   default ''     COMMENT '手机号',
`remark`        text                         COMMENT '备注',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='招募信息' AUTO_INCREMENT=1 ;

-- 找医找药
drop table if EXISTS `yoshop_see_docker`;
CREATE TABLE `yoshop_see_docker` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户(推荐者)id',

`name`          varchar(20)   default ''     COMMENT '姓名',
`mobile`        varchar(20)   default ''     COMMENT '手机号',
`province`      varchar(20)   not null       COMMENT '省份',
`city`          varchar(20)   not null       COMMENT '城市',
`area`          varchar(20)   not null       COMMENT '城区',
`type_illness`  varchar(20)   default ''             COMMENT '疾病类型 归属于 例如“健康志愿者”，“骨质疏松患者”等的id',
`remark`        text                          COMMENT '备注',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='找医找药' AUTO_INCREMENT=1 ;


-- 项目列表
drop table if EXISTS `yoshop_project`;
CREATE TABLE `yoshop_project` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',

`title`         varchar(20)   default ''     COMMENT '项目标题',
`type_project`  tinyint(1)    default 1     COMMENT '项目类型 1患者项目，2健康项目',
`type_illness`  varchar(20)   default ''     COMMENT '疾病类型 归属于 例如“健康志愿者”，“骨质疏松患者”等的id',
`allowance`         decimal(11,2)  unsigned  default 0   COMMENT '营养补助',
`out_award`         decimal(11,2)  unsigned  default 0   COMMENT '出组奖励（自主报名参加奖励的金币）',
`gold`            decimal(11,2)  unsigned  default 0   COMMENT '营养补助对应的金币(推荐者奖励金币)',
`transport_gold`  decimal(11,2)  unsigned  default 0   COMMENT '交通补助',
`start_time`      int(11)       unsigned      DEFAULT NULL  COMMENT '活动开始时间',
`end_time`      int(11)       unsigned      DEFAULT NULL  COMMENT '活动截止时间',
`sex`           tinyint(1)    unsigned      default 1    COMMENT '限制性别 0不限制，1男，2女',
`age_min`     int(11)       unsigned      DEFAULT NULL  COMMENT '最小年龄',
`age_max`     int(11)       unsigned      DEFAULT NULL  COMMENT '最大年龄',
`is_smoke`      tinyint(1)    default 1     COMMENT '吸烟检查 1有烟检，0无烟检',
`be_in_day`     int(11)       default 0     COMMENT '住院天数，0不需住院',
`hospital_id`      varchar(50)   default ''     COMMENT '实验医院id，例如2',
`hospital_id_other`     varchar(200)   default ''     COMMENT '所有实验医院id，例如多家医院json字符格式[1,2,22,3]',
`intro`              text                       COMMENT '简介',
`arrangement`        text                       COMMENT '时间安排说明',
`earnings_remark`    text                       COMMENT '实验获益说明',
`scan`             int(11)       unsigned      DEFAULT 0  COMMENT '浏览量',
`apply_count`      int(11)       unsigned      DEFAULT 0  COMMENT '报名人数',
`province`      varchar(20)   not null       COMMENT '省份',
`city`          varchar(20)   not null       COMMENT '城市',
`area`          varchar(20)   not null       COMMENT '城区',
`is_heat`       tinyint(1)    default 0      COMMENT '1热门项目 0普通项目',
`create_day`    varchar(20)   not null       COMMENT '创建日期，格式20211006',
`copy`              text                     COMMENT '复制文本',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='项目列表' AUTO_INCREMENT=1 ;

-- 用户参与的项目列表
drop table if EXISTS `yoshop_project_part`;
CREATE TABLE `yoshop_project_part` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',
`user_id`       int(50)       unsigned       default 0  COMMENT '用户id',

`patient_id`        int(50)       unsigned       default 0  COMMENT '患者人群表id，根据项目健康或者患者项目类来判断哪一个表是yoshop_customer_patient或yoshop_customer，值为0表该项不存在',
`customer_id`        int(50)       unsigned       default 0  COMMENT '健康人群表id，根据项目健康或者患者项目类来判断哪一个表是yoshop_customer_patient或yoshop_customer，值为0表该项不存在',
`order_no`      varchar(32)   not null       COMMENT '订单号',
`pid`           int(50)       unsigned       default 0  COMMENT '推荐者id',
`project_id`    int(20)    not null      COMMENT '项目列表id',
`part_type`     tinyint(3) unsigned      default 0     COMMENT '用户参与项目方式 0自主参与 1推荐',
`status`        tinyint(3) unsigned      default 1     COMMENT '项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7待完成 8已完成',
`audit_status`        tinyint(1) unsigned      default 0     COMMENT '审核状态 0未审核 1已审核',
`sign_status`         tinyint(1) unsigned      default 0     COMMENT '签到状态 0未签到 1已签到',
`informed_status`     tinyint(1) unsigned      default 0     COMMENT '知情状态 0未知情 1已知情',
`filter_status`       tinyint(1) unsigned      default 0     COMMENT '筛选状态 0筛选失败 1筛选成功',
`group_status`        tinyint(1) unsigned      default 0     COMMENT '入组状态 0未入组 1已入组',
`test_status`         tinyint(1) unsigned      default 0     COMMENT '实验状态 0实验中脱离 1已完成实验',
`finish_status`       tinyint(1) unsigned      default 0     COMMENT '完成状态 0未完成 1已完成',
`audit_time`    int(11)    unsigned      default 0  COMMENT '审核时间',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
key  `user_id`    (`user_id`),
key  `pid`        (`pid`),
KEY  `store_id`   (`store_id`),
KEY  `project_id` (`project_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户参与的项目列表' AUTO_INCREMENT=1 ;


-- 所有医院
drop table if EXISTS `yoshop_hospital`;
CREATE TABLE `yoshop_hospital` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',

`province`      varchar(20)   not null       COMMENT '省份',
`city`          varchar(20)   not null       COMMENT '城市',
`area`          varchar(20)   not null       COMMENT '城区',
`name`          varchar(50)   not null       COMMENT '医院名称',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='所有医院' AUTO_INCREMENT=1 ;

-- 疾病类型
drop table if EXISTS `yoshop_illness`;
CREATE TABLE `yoshop_illness` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',

`tag`           varchar(50)   not null       COMMENT '疾病标签名，例如“健康志愿者”，“骨质疏松患者”等',

`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='疾病类型' AUTO_INCREMENT=1 ;

-- 消息通知
drop table if EXISTS `yoshop_msg`;
CREATE TABLE `yoshop_msg` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',

`user_id`       int(50)       unsigned       default 0  COMMENT '用户id',
`msg`         varchar(50)   not null       COMMENT '消息内容',
`class_msg`    tinyint(1) unsigned      default 1     COMMENT '消息分类 1公布消息 2系统消息',
`class_type`   tinyint(1) unsigned      default 1     COMMENT '消息种类 1普通消息 2积分消息 3金币消息',


`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
KEY  `class_msg`   (`class_msg`),
KEY  `class_type`   (`class_type`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='消息通知' AUTO_INCREMENT=1 ;

-- 项目收藏
drop table if EXISTS `yoshop_collect`;
CREATE TABLE `yoshop_collect` (
`id`            int(11)       not null       AUTO_INCREMENT,
`store_id`      int(11)       unsigned       default 0  COMMENT '商城ID',

`user_id`       int(50)       unsigned       default 0  COMMENT '用户id',
`project_id`         varchar(50)   not null       COMMENT '收藏的项目id',


`is_delete`     tinyint(3) unsigned      default 0     COMMENT '是否删除',
`create_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '创建时间',
`update_time`   int(11)    unsigned      DEFAULT NULL  COMMENT '更新时间',

PRIMARY KEY       (`id`),
KEY  `project_id`   (`project_id`),
KEY  `store_id`   (`store_id`)
)ENGINE=InnoDB CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='项目收藏' AUTO_INCREMENT=1 ;

-- 清理会员信息和金币记录-开发时用
TRUNCATE `zhaomu`.`yoshop_degain`;
TRUNCATE `zhaomu`.`yoshop_dermember`;
TRUNCATE `zhaomu`.`yoshop_Derrecord`;
TRUNCATE `zhaomu`.`yoshop_user_points_log`;
#update `zhaomu`.`yoshop_user` set points=0;

