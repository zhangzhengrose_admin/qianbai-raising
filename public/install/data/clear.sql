-- 清理经销商成员、会员和授权数据

TRUNCATE `yoshop`.`yoshop_dermember`;
TRUNCATE `yoshop`.`yoshop_user`;
TRUNCATE `yoshop`.`yoshop_user_oauth`;
