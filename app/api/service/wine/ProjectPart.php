<?php
namespace app\api\service\wine;

use app\common\service\BaseService;
use app\api\model\wine\Dermember as DermemberModel;
use app\api\model\wine\ProjectPart as ProjectPartService;

class ProjectPart extends BaseService{
    // 参加项目
    public static function partProject(int $project_id,int $user_id,int $pid=0,int $id=0){
        // 判断项目参与方式  0自主参与 1推荐
        if($pid == 0){
            $part_type = 0;
            $wPid = DermemberModel::getPid($user_id);
        }else{
            $part_type = 1;
            $wPid = $pid;
        }
        return ProjectPartService::partIn($user_id,$project_id,$part_type,$wPid,$id);
    }
}
