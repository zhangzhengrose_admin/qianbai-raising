<?php
namespace app\api\service\wine;

use app\api\model\wine\Degain;
use app\api\model\wine\Derrecord;
use app\common\model\store\Setting as SettingModel;
use app\common\service\BaseService;
use app\api\model\wine\Dermember as DerModel;
use app\common\model\user as UserModel;
use app\common\service\Tempmsg;
use app\store\model\user\PointsLog as PointsLogModel;

class Dermember extends BaseService{
    /**
     * 分佣服务
     * @param int $user_id
     * @param int $gain_type
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function distribution(int $user_id,int $gain_type):void
    {
        $config = SettingModel::getItem('gold', getStoreId());
        $records = [
            1 => $config['refReward1'],
            2 => $config['refReward2'],
            3 => $config['refReward3'],
        ];
        $shareReward = (float)$config['shareReward'];
        // 不论何种情况都要绑定
        self::addRecommend($user_id,$records,1,3);
        if($gain_type == 2){
            // 分享途径
            self::addShare($user_id,$shareReward);
        }
        // 注册送积分
        self::addPointsRegister($user_id);
    }

    /**
     * 添加推荐记录
     * @param int $user_id
     * @param array $records 积分收益
     * @param int $gain_type 收益类型1推荐奖励,2分享奖励
     * @param int $level 分销级别，默认三级分销
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function addRecommend(int $user_id,array $records,int $gain_type,int $level = 3):void
    {
        $recordArr = [];
        DerModel::getRecommend($user_id,$recordArr, $level);
        foreach ($recordArr as $mLevel => $record){
            // $mLevel 从1开始
            $earnings = (float)$records[$mLevel];
            $pid = (int)$record['user_id'];
            if($mLevel == 1){
                // 直接上线获得邀请关注积分
                self::addPointsShared($pid);
            }
            // 我的下线人数+1
            self::pcountAdd($pid);
            // 添加推荐记录
            Degain::AddRecommend($pid,$user_id,$earnings,$gain_type);
            // 添加经销商推荐记录
            Derrecord::add($pid,$user_id,$mLevel);
        }
    }

    /**
     * 添加分享记录
     * @param int $user_id
     * @param float $earnings
     * @param int $gain_type
     */
    public static function addShare(int $user_id,float $earnings,int $gain_type = 2):void
    {
        $pid = DerModel::getPid($user_id);
        Degain::AddRecommend($pid,$user_id,$earnings,$gain_type);
    }

    // 注册送积分
    public static function addPointsRegister(int $user_id):void
    {
        $config = SettingModel::getItem('points', getStoreId());
        $points_register = (float) $config['points_register'];
        self::rechargeToPoints($user_id,$points_register,'inc','积分营销-注册送积分');
        // 积分通知
        Tempmsg::setIntegralMsg($points_register,$user_id);
    }
    // 每日登陆(签到)送积分
    public static function addPointsSign(string $user_id):void
    {
        $config = SettingModel::getItem('points', getStoreId());
        $points_every_login = (float) $config['points_every_login'];
        self::rechargeToPoints($user_id,$points_every_login,'inc','积分营销-每日登陆送积分');
        // 积分通知
        Tempmsg::setIntegralMsg($points_every_login,$user_id);
    }
    // 邀请奖励送积分
    public static function addPointsShared(string $user_id):void
    {
        $config = SettingModel::getItem('points', getStoreId());
        $points_share = (float) $config['points_share'];
        self::rechargeToPoints($user_id,$points_share,'inc','积分营销-邀请奖励送积分');
        // 积分通知
        Tempmsg::setIntegralMsg($points_share,$user_id);
    }
    // 活动奖励送积分
    public static function addPointsAction(string $user_id):void
    {
        $config = SettingModel::getItem('points', getStoreId());
        $points_action = (float) $config['points_action'];
        self::rechargeToPoints($user_id,$points_action,'inc','积分营销-活动奖励送积分');
        // 积分通知
        Tempmsg::setIntegralMsg($points_action,$user_id);
    }

    // 添加减少积分
    private static function rechargeToPoints(int $user_id,float $value= 0,string $mode = 'inc', string $describe = '积分营销',float $points = 0){
        if ($value < 0) {
            throwError('请输入正确的积分数量',500);
        }
        // 判断充值方式，计算最终积分
        if ($mode === 'inc') {
            $diffMoney = $value;
        } elseif ($mode === 'dec') {
            $diffMoney = - $value;
        } else {
            $diffMoney = $value - $points;
        }
        // 更新记录
        return UserModel::setIncPoints($user_id,$diffMoney,$describe);
    }

    // 下线人数+1
    private static function pcountAdd(int $pid){
        return DerModel::setPcount($pid,1,'inc');
    }
}
