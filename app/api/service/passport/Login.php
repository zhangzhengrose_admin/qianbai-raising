<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types=1);

namespace app\api\service\passport;

use app\api\model\User;
use app\api\model\User as UserModel;
use app\api\service\user\Oauth;
use app\api\service\user\Oauth as OauthService;
use app\api\service\user\Avatar as AvatarService;
use app\api\validate\passport\Login as ValidateLogin;
use app\common\exception\BaseException;
use app\common\library\helper;
use app\common\service\BaseService;
use edward\captcha\facade\CaptchaApi;
use think\facade\Cache;

/**
 * 服务类：用户登录
 * Class Login
 * @package app\api\service\passport
 */
class Login extends BaseService
{
    // 用户信息 (登录成功后才记录)
    private $userInfo;
    // 用户初始密码值
    private $pwd = 'q1318729662';

    // 用于生成token的自定义盐
    const TOKEN_SALT = 'user_salt';

    /**
     * 执行用户登录
     * @param array $data
     * @param bool $checkSms
     * @return bool
     * @throws BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login(array $data,bool $checkSms=true)
    {
        // 无密码则设置初始密码
        if(empty($data['pwd'])){
            $data['pwd'] = $this->pwd;
        }
        // 数据验证
        if (!$this->validate($data,$checkSms)) {
            return false;
        }
        // 自动登录注册
        $r = $this->register($data);
        if($r===false){
            return false;
        }
        // 保存oauth信息
        $this->oauth($data);
        // 记录登录态
        return $this->session();
    }

    /**
     * 绑定手机号
     * @throws BaseException
     */
    public function bindMobile(array $data,bool $checkSms=true){
        // 无密码则设置初始密码
        if(empty($data['pwd'])){
            $data['pwd'] = $this->pwd;
        }
        // 数据验证
        if (!$this->validate($data,$checkSms)) {
            return false;
        }

        $wxInfo = $this->getBindWxInfo($data);
        // 自动登录注册
        $r = $this->registerMobile($data,$wxInfo);
        if($r===false){
            return false;
        }
        // 保存oauth信息
        $oauthInfo = [
            'oauth_id' => $wxInfo['openid'],
            'unionid'  => $wxInfo['unionid'] ?? ""
        ];
        $this->oauthMobile($data,$oauthInfo);
        // 记录登录态
        return $this->session();
    }

    /**
     * 快捷登录：微信小程序用户
     * @param array $data
     * @return bool
     * @throws BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function mpWxLogin(array $data)
    {
        try {
            // 根据code换取openid
            $wxSession = OauthService::wxCode2Session($data['code']);
        } catch (BaseException $e) {
            // showError参数表示让前端显示错误
            throwError($e->getMessage(), null, ['showError' => true]);
            return false;
        }
        // 判断openid是否存在
        $userId = OauthService::getUserIdByOauthId($wxSession['openid'], 'MP-WEIXIN');
        // 获取用户信息
        $userInfo = !empty($userId) ? UserModel::detail($userId) : null;
        if (empty($userId) || empty($userInfo)) {
            $this->error = '第三方用户不存在';
            return false;
        }
        // 更新用户登录信息
        $this->updateUser($userInfo, true, $data);
        // 记录登录态
        return $this->session();
    }

    /**
     * 快捷登录：微信公众号用户
     * @param array $data
     * @return bool
     * @throws BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function gWxLogin(array $data){
        try{
            $wxInfoArr = OauthService::wxgCode2OpenidWxinfo($data['code']);
            $openid = $wxInfoArr['openid'];
            $wxInfo = $wxInfoArr['wxInfo'];
            $bindId = $this->setBindId($wxInfo);
        }catch (BaseException $e){
            // showError参数表示让前端显示错误
            throwError($e->getMessage(), null, ['showError' => true]);
            return false;
        }
        // 判断openid是否存在
        $userId = OauthService::getUserIdByOauthId($openid, 'G-WEIXIN');
        // 获取用户信息
        $userInfo = !empty($userId) ? UserModel::detail($userId) : null;
        if (empty($userId) || empty($userInfo)) {
            $this->error = '第三方用户不存在';
            $this->errorData = $bindId;
            return false;
        }
        $partyData = [
            'userInfo' => [
                'nickName' => base64_encode($wxInfo['nickname']),
                'gender' => $wxInfo['sex'],
                'avatarUrl' => $wxInfo['headimgurl']
            ]
        ];
        // 更新用户登录信息
        $this->userInfo = $userInfo;
        $dataLogin = $this->partyUserInfo($partyData);
        $dataLogin['country'] = $wxInfo['country'];
        $dataLogin['province'] = $wxInfo['province'];
        $dataLogin['city'] = $wxInfo['city'];
        $r = $this->userInfo->save($dataLogin);
        if($r===false){
            $this->error = '公众号用户数据更新失败';
            return false;
        }
        // 记录登录态
        return $this->session();
    }

    /**
     * 设置微信用户绑定信息
     * @param array $wxInfo
     * @return false|string
     */
    private function setBindId(array $wxInfo){
        $content = json_encode($wxInfo);
        $bindId = md5($content);
        if(Cache::set($bindId,$content,300)){
            return $bindId;
        }
        return false;
    }

    /**
     * 获取微信用户绑定信息
     * @param array $data
     * @return array
     * @throws BaseException
     */
    private function getBindWxInfo(array $data){
        $bindId = $data['bindId'];
        $val = Cache::pull($bindId);
        if($val === null){
            throwError('bindId已失效,请重新获取');
        }
        return  helper::jsonDecode($val);
    }

    /**
     * 保存oauth信息
     * @param array $data
     * @return bool
     * @throws BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    private function oauth(array $data)
    {
        if ($data['isParty']) {
            $Oauth = new OauthService;
            return $Oauth->party((int)$this->userInfo['user_id'], $data['partyData']);
        }
        return true;
    }

    /**
     * 保存oauth信息
     * @param array $data
     * @return void
     * @throws BaseException
     */
    private function oauthMobile(array $data,array $oauthInfo): void
    {
        if ($data['isParty']) {
            $Oauth = new OauthService;
            $Oauth->partyMobile((int)$this->userInfo['user_id'], $data['partyData'],$oauthInfo);
        }
    }

    /**
     * 当前登录的用户信息
     * @return array
     */
    public function getUserInfo()
    {
        return $this->userInfo;
    }

    /**
     * 自动登录注册
     * @param array $data
     * @return bool
     */
    private function register(array $data)
    {
        // 查询用户是否已存在
        $userInfo = UserModel::detail(['mobile' => $data['mobile']]);
        if ($userInfo) {
            // 用户存在: 更新登录信息
            return $this->updateUser($userInfo, $data['isParty'], $data['partyData']);
        } else {
            if (!isset($data['pwd']) || empty($data['pwd'])){
                $this->error = '密码必须';
                return false;
            }
            // 用户不存在: 新增用户
            return $this->createUser($data['mobile'], $data['pwd'], $data['isParty'], $data['partyData']);
        }
    }

    private function registerMobile(array $data,array $wxInfo)
    {
        // 查询用户是否已存在
        $userInfo = UserModel::detail(['mobile' => $data['mobile']]);
        $data['partyData']['userInfo'] = [
            'nickName' => base64_encode($wxInfo['nickname']),
            'gender' => $wxInfo['sex'],
            'avatarUrl' => $wxInfo['headimgurl']
        ];
        if ($userInfo) {
            // 用户存在: 更新登录信息
            return $this->updateUser($userInfo, $data['isParty'], $data['partyData'],'G-WEIXIN');
        } else {
            if (!isset($data['pwd']) || empty($data['pwd'])){
                $this->error = '密码必须';
                return false;
            }
            // 用户不存在: 新增用户
            return $this->createUser($data['mobile'], $data['pwd'], $data['isParty'], $data['partyData'],'G-WEIXIN');
        }
    }


    /**
     * 新增用户
     * @param string $mobile 手机号
     * @param bool $isParty 是否存在第三方用户信息
     * @param array $partyData 用户信息(第三方)
     * @return bool
     */
    private function createUser(string $mobile, string $pwd, bool $isParty, array $partyData = [],string $oauthType = 'MP-WEIXIN')
    {
        // 用户信息
        $data = [
            'mobile' => $mobile,
            'pwd' => md5($pwd),
            'nick_name' => hide_mobile($mobile),
            'platform' => getPlatform(),
            'last_login_time' => time(),
            'store_id' => $this->storeId
        ];
        // 写入用户信息(第三方)
        if ($isParty === true && !empty($partyData) && isset($partyData['oauth']) && $partyData['oauth'] == $oauthType) {
            $partyUserInfo = $this->partyUserInfo($partyData, true);
            $data = array_merge($data, $partyUserInfo);
        }
        // 新增用户记录
        $model = new UserModel;
        $status = $model->save($data);
        // 记录用户信息
        $this->userInfo = $model;
        return $status;
    }

    /**
     * 第三方用户信息
     * @param array $partyData 第三方用户信息
     * @param bool $isGetAvatarUrl 是否下载头像
     * @return array
     */
    private function partyUserInfo(array $partyData, bool $isGetAvatarUrl = true)
    {
        $partyUserInfo = $partyData['userInfo'];
        $data = [
            'nick_name' => $partyUserInfo['nickName'],
            'gender' => $partyUserInfo['gender']
        ];
        // 下载用户头像
        if ($isGetAvatarUrl) {
            $data['avatar_id'] = $this->partyAvatar($partyUserInfo['avatarUrl']);
        }
        return $data;
    }

    /**
     * 下载第三方头像并写入文件库
     * @param string $avatarUrl
     * @return int
     */
    private function partyAvatar(string $avatarUrl)
    {
        $Avatar = new AvatarService;
        $fileId = $Avatar->party($avatarUrl);
        return $fileId ? $fileId : 0;
    }

    /**
     * 更新用户登录信息
     * @param UserModel $userInfo
     * @param bool $isParty 是否存在第三方用户信息
     * @param array $partyData 用户信息(第三方)
     * @return bool
     */
    private function updateUser(UserModel $userInfo, bool $isParty, array $partyData = [],string $oauthType = 'MP-WEIXIN')
    {
        // 用户信息
        $data = [
            'last_login_time' => time(),
            'store_id' => $this->storeId
        ];
        // 写入用户信息(第三方)
        if ($isParty === true && !empty($partyData) && isset($partyData['oauth']) && $partyData['oauth'] == $oauthType) {
            $partyUserInfo = $this->partyUserInfo($partyData, !$userInfo['avatar_id']);
            $data = array_merge($data, $partyUserInfo);
        }
        // 更新用户记录
        $status = $userInfo->save($data) !== false;
        // 记录用户信息
        $this->userInfo = $userInfo;
        return $status;
    }

    /**
     * 记录登录态
     * @return bool
     * @throws BaseException
     */
    private function session()
    {
        empty($this->userInfo) && throwError('未找到用户信息');
        // 登录的token
        $token = $this->getToken((int)$this->userInfo['user_id']);
        // 记录缓存, 30天
        Cache::set($token, [
            'user' => $this->userInfo,
            'store_id' => $this->storeId,
            'is_login' => true,
        ], 86400 * 30);
        return true;
    }

    /**
     * 数据验证
     * @param array $data
     * @param bool $checkSms
     * @return bool
     */
    private function validate(array $data, bool $checkSms)
    {
        // 数据验证
        $validate = new ValidateLogin;
        if (!$validate->check($data)) {
            $this->error = $validate->getError();
            return false;
        }

        if($checkSms){
            // 需要验证短信的情况
            // 判断用户是否存在
            if(User::checkUserIsExists($data['mobile'])){
                // 用户存在则验证短信验证码是否匹配
                if (isset($data['smsCode']) && !empty($data['smsCode']) && !CaptchaApi::checkSms($data['smsCode'], $data['mobile'])) {
                    $this->error = '短信验证码不正确';
                    return false;
                }
                // 验证密码是否匹配
                if (isset($data['pwd']) && !empty($data['pwd']) && !User::checkPassword($data['mobile'],$data['pwd'])) {
                    $this->error = '账号或密码不正确';
                    return false;
                }
            }else{
                if(empty($data['smsCode']) || empty($data['pwd'])){
                    $this->error = '验证码或密码不能为空';
                    return false;
                }
                if(!CaptchaApi::checkSms($data['smsCode'], $data['mobile'])){
                    $this->error = '短信验证码不正确';
                    return false;
                }
                if(strlen($data['pwd'])<=6){
                    $this->error = '密码长度必须大于6位';
                    return false;
                }
            }
        }else{
            // 不需要验证短信的情况
            // 判断用户是否存在
            if(User::checkUserIsExists($data['mobile'])){
                // 验证密码是否匹配
                if (isset($data['pwd']) && !empty($data['pwd']) && !User::checkPassword($data['mobile'],$data['pwd'])) {
                    $this->error = '账号或密码不正确';
                    return false;
                }
            }else{
                if(empty($data['pwd'])){
                    $this->error = '密码不能为空';
                    return false;
                }
                if(strlen($data['pwd'])<=6){
                    $this->error = '密码长度必须大于6位';
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 获取登录的token
     * @param int $userId
     * @return string
     */
    public function getToken(int $userId)
    {
        static $token = '';
        if (empty($token)) {
            $token = $this->makeToken($userId);
        }
        return $token;
    }

    /**
     * 生成用户认证的token
     * @param int $userId
     * @return string
     */
    public function makeToken(int $userId)
    {
        $storeId = $this->storeId;
        // 生成一个不会重复的随机字符串
        $guid = get_guid_v4();
        // 当前时间戳 (精确到毫秒)
        $timeStamp = microtime(true);
        // 自定义一个盐
        $salt = self::TOKEN_SALT;
        return md5("{$storeId}_{$timeStamp}_{$userId}_{$guid}_{$salt}");
    }

}
