<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Illness extends BaseModel{
    protected $autoWriteTimestamp = true;

    // 获取疾病类型列表
    public static function getList(){
        return self::where(['is_delete'=>0])
            ->field('id,store_id,tag')
            ->paginate(15);
    }
}
