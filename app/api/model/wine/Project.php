<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Project extends BaseModel{
    protected $autoWriteTimestamp = true;

    // 关联疾病类型
    public function illness(){
        return $this->hasOne(Illness::class,'id','type_illness')
            ->bind(['tag']);
    }

    // 关联所属医院
    public function hospital(){
        return $this->hasOne(Hospital::class,'id','hospital_id')
            ->bind([
                'province_h'=>'province',
                'city_h'=>'city',
                'area_h'=>'area',
                'name_h'=>'name'
            ]);
    }

    // 查询项目
    public static function getList(array $data){
        $where = [];
        $current_time = time();
        // 按创建日期选择
        if(isset($data['create_day'])){
            $where['create_day'] = $data['create_day'];
        }
        // 根据地区进行选择
        if(isset($data['province']) && isset($data['city']) && isset($data['area'])){
            $where['province'] = $data['province'];
            $where['city'] = $data['city'];
            $where['area'] = $data['area'];
        }
        // 补助大于多少金额
        if(isset($data['allowance'])){
            $where['allowance'] = ['>=',$data['allowance']];
        }
        // 周期(天)
        if(isset($data['scope_day'])){
            $where['create_day'] = ['>=',date('Ymd',$current_time)+$data['scope_day']];
        }
        // 是否热门项目
        if (isset($data['is_heat'])){
            $where['is_heat'] = 1;
        }

        return self::where(['is_delete'=>0])
            ->with(['illness','hospital'])
            ->where('end_time','>',$current_time)
            ->where($where)
            ->paginate(15);
    }

    // 获取项目单条记录
    public static function detail(int $id){
        $r = self::where(['id'=>$id])
            ->with(['illness','hospital'])
            ->find();
        if($r->isEmpty()){
            throwError('没有找到项目');
        }
        return $r;
    }
}
