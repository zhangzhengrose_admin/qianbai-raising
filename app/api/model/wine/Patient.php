<?php
 namespace app\api\model\wine;

 use app\common\library\helper;
 use app\common\model\BaseModel;

 class Patient extends BaseModel{
     protected $autoWriteTimestamp = true;

     protected $name = 'customer_patient';

     public static function add(array $params):int
     {
         // 以手机号码为唯一值
         $isEmpty = self::where(['mobile'=>$params['mobile']])->findOrEmpty();
         if($isEmpty->isEmpty()){
             $data = [
                 'store_id' => getStoreId(),
                 'user_id' => $params['user_id'],
                 'name' => $params['name'],
                 'mobile' => $params['mobile'],
                 'idCard' => $params['idCard'],
                 'birthday' => helper::checkTimer($params['birthday']),
                 'sex' => $params['sex'],
                 'age' => $params['age'],
                 'province' => $params['province'],
                 'city' => $params['city'],
                 'area' => $params['area'],
                 'type_illness' => $params['type_illness'],
                 'case' => htmlspecialchars_decode($params['case']),
                 'remark' => $params['remark']
             ];
             $r = self::create($data);
             return $r->id;
         }else{
             $data = [
                 'store_id' => getStoreId(),
                 'user_id' => $params['user_id'],
                 'name' => $params['name'],
                 'idCard' => $params['idCard'],
                 'birthday' => helper::checkTimer($params['birthday']),
                 'sex' => $params['sex'],
                 'age' => $params['age'],
                 'province' => $params['province'],
                 'city' => $params['city'],
                 'area' => $params['area'],
                 'type_illness' => $params['type_illness'],
                 'case' => htmlspecialchars_decode($params['case']),
                 'remark' => $params['remark']
             ];
             self::update($data,['mobile'=>$params['mobile']]);
             return self::where(['mobile'=>$params['mobile']])->value('id');
         }

     }
 }
