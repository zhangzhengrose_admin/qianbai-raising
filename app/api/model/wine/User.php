<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class User extends BaseModel{
    public function dermerber(){
        return $this->hasOne(Dermember::class,'user_id','user_id')
            ->bind(['pcount']);
    }

    //查询用户信息
    public static function getCurrentCondition(int $user_id,int $grade_id){
        $model = self::where(['user_id'=>$user_id,'grade_id'=>$grade_id])
            ->findOrEmpty();
        if($model->isExists()){
            throwError('你是该级别会员，请勿重复申请',500);
        }
        // 不存在会员级别才开始申请
        return $model->with(['dermerber'])
            ->find();
    }
}
