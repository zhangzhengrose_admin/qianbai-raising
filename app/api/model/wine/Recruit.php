<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Recruit extends BaseModel{
    protected $autoWriteTimestamp = true;

    public static function add(array $params):bool
    {
        $data = [
            'store_id' => getStoreId(),
            'user_id' => $params['user_id'],
            'name' => $params['name'],
            'mobile' => $params['mobile'],
            'remark' => $params['remark']
        ];
        return (new static)->save($data);
    }
}
