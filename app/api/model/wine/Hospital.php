<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Hospital extends BaseModel{
    protected $autoWriteTimestamp = true;

    // $idStr = '1,2,3,22'
    public static function getList(string $idStr = ''){
        if(empty($idStr)){
            return self::where(['is_delete'=>0])
                ->paginate(15);
        }else{
            return self::where(['is_delete'=>0])
                ->whereIn('id',$idStr)
                ->paginate(15);
        }
    }
}
