<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;
use app\common\service\Order as OrderService;
use app\store\model\project\CustomerPatient;

class ProjectPart extends BaseModel{
    protected $autoWriteTimestamp = true;

    public static function partIn(int $user_id,int $project_id,int $part_type,int $pid=0,int $id=0){
        // 判断项目是否存在
        $project_r = Project::where('id','=',$project_id)->findOrEmpty();
        if($project_r->isEmpty()){
            throwError('不存在该项目，无法报名',500);
        }
        // 判断是否报名
        $isPartIn = self::where(['user_id' => $user_id, 'project_id'=>$project_id])->findOrEmpty();
        if($isPartIn->isExists()){
            throwError('你已经报名',500);
        }else{
            // 查询项目是健康项目还算患者项目
            $type_project = Project::where(['id'=>$project_id])->value('type_project');
            if($type_project == 1){
                // 1患者项目
                return self::create([
                    'store_id' => getStoreId(),
                    'user_id' => $user_id,
                    'project_id' => $project_id,
                    'part_type' => $part_type,
                    'pid' => $pid,
                    // 生成订单号
                    'order_no' => OrderService::createOrderNo(),
                    'patient_id'=> $id
                ]);
            }else if($type_project == 2){
                // 2健康项目
                return self::create([
                    'store_id' => getStoreId(),
                    'user_id' => $user_id,
                    'project_id' => $project_id,
                    'part_type' => $part_type,
                    'pid' => $pid,
                    // 生成订单号
                    'order_no' => OrderService::createOrderNo(),
                    'customer_id'=> $id
                ]);
            }else{
                throwError('未定义的项目，无法报名',500);
            }
        }
        return false;
    }

    // 我的推荐项目
    public static function myProject(int $user_id,int $status)
    {
        $where = [];
        if($status>0){
            $where['status'] = $status;
        }
        return self::where(['pid' => $user_id,'is_delete'=>0])
            ->where($where)
            ->order('create_time', 'desc')
            ->paginate(15);
    }

    // 管理项目标
    public function project(){
        return $this->hasOne(Project::class,'id','project_id')
            ->bind(['title']);
    }

    // 关联健康表
    public function customer(){
        return $this->hasOne(Customer::class,'id','customer_id');
    }

    // 关联患者表
    public function patient(){
        return $this->hasOne(CustomerPatient::class,'id','patient_id');
    }

    // 我报名的项目
    public static function myPartInList(int $user_id,int $status){
        $where = [];
        if($status>0){
            $where['status'] = $status;
        }
        return self::where(['user_id' => $user_id,'is_delete'=>0])
            ->where($where)
            ->with(['project','customer','patient'])
            ->order('create_time', 'desc')
            ->paginate(15);
    }

    // 我的报名统计
    public static function statistics(int $user_id){
        // 我推荐用户个数
        $shareCount = self::where(['pid'=>$user_id])->count();
        // 我推荐用户已完成个数
        $overCount = self::where(['pid'=>$user_id,'status'=>8])->count();
        return ['shareCount'=>$shareCount,'overCount'=>$overCount];
    }
}
