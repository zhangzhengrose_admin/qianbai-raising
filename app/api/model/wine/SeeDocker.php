<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class SeeDocker extends BaseModel{
    protected $autoWriteTimestamp = true;

    public static function add(array $params):bool
    {
        $data = [
            'store_id' => getStoreId(),
            'user_id' => $params['user_id'],
            'name' => $params['name'],
            'mobile' => $params['mobile'],
            'remark' => $params['remark'],

            'province' => $params['province'],
            'city' => $params['city'],
            'area' => $params['area'],
            'type_illness' => $params['type_illness']
        ];
        return (new static)->save($data);
    }
}
