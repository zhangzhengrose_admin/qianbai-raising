<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class DegainCash extends BaseModel{
    protected $autoWriteTimestamp = true;

    // 创建金币支取记录
    public static function buildRecord(int $user_id,float $earnings,int $cash_type){
        return self::create([
            'store_id' => getStoreId(),
            'user_id' => $user_id,
            'cash_type' => $cash_type,
            'earnings' =>  $earnings
        ]);
    }

    // 获取金币支取列表
    public static function getList(int $user_id){
        return self::where(['user_id'=>$user_id,'is_delete'=>0])
            ->paginate(15);
    }
}
