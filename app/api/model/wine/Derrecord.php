<?php
namespace app\api\model\wine;

use app\common\model\BaseModel;

class Derrecord extends BaseModel{
    protected $autoWriteTimestamp = true;

    // 添加经销商记录
    public static function add(int $user_id,int $mid,int $level){
        return self::create([
            'store_id' => getStoreId(),
            'user_id' => $user_id,
            'level' => $level,
            'mid' => $mid
        ]);
    }

    /**
     * 关联用户表
     * @return \think\model\relation\HasOne
     */
    public function user(){
        return $this->hasOne(\app\common\model\User::class,'user_id','mid')
            ->with('avatar')
            ->bind(['nick_name','avatar_url','gender']);
    }

    /**
     * 关联经销商表
     * @return \think\model\relation\HasOne
     */
    public function Dermember(){
        return $this->hasOne(Dermember::class,'user_id','user_id')
            ->bind(['pcount']);
    }

    // 查询下级经销商
    public static function findDear(int $user_id,int $level = 0){
        $where['user_id'] = $user_id;
        if($level > 0){
            $where['level'] = $level;
        }
        return self::where($where)
            ->with(['Dermember','user'])
            ->paginate(15);
    }
}
