<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\Member as MemberModel;
use app\api\validate\wine\Member as MemberValidate;
use think\App;

class Member extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 个人信息
    public function detail(){
        $detail = MemberModel::detail($this->user_id);
        return $this->renderSuccess(compact('detail'),'个人信息获取成功');
    }

    // 添加个人信息
    public function addMember(){
        $params = $this->postForm();
        $MemberValidate = new MemberValidate();
        if(!$MemberValidate->scene('add')->check($params)){
            return $this->renderError($MemberValidate->getError());
        }
        $params['user_id'] = $this->user_id;
        if(!MemberModel::add($params)){
            return $this->renderError('添加个人信息失败');
        }
        return $this->renderSuccess('添加个人信息成功');
    }

    // 修改个人信息
    public function editMember(){
        $params = $this->postForm();
        $params['user_id'] = $this->user_id;
        if(!MemberModel::edit($params)){
            return $this->renderError('编辑个人信息失败');
        }
        return $this->renderSuccess('编辑个人信息成功');
    }
}
