<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\DegainCash;
use think\App;

class Cash extends Controller{
    private $user_id;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 申请提现
    public function apply(){
        $params = $this->postForm();
        $cash_type = $params['cash_type'] ?? 1;
        if ($cash_type != 1){
            throwError('暂时只支持提现至微信',500);
        }
        if(!isset($params['cash_gold'])){
            throwError('缺少参数cash_gold',500);
        }
        $gold_balance = \app\api\model\wine\Dermember::getGoldBalance($this->user_id);
        $cash_gold = $params['cash_gold'];
        if($cash_gold>$gold_balance){
            throwError('你可提取金币不够',500);
        }
        \app\api\model\wine\Dermember::setCashGold($this->user_id,$cash_gold,$cash_type);
        return $this->renderSuccess('成功获取可提现金币');
    }

    // 可提现金币
    public function goldInfo(){
        $gold_balance = \app\api\model\wine\Dermember::getGoldBalance($this->user_id);
        return $this->renderSuccess(compact('gold_balance'),'成功获取可提现金币');
    }

    // 我的金币提取记录
    public function cashGoldList(){
        $list = DegainCash::getList($this->user_id);
        return $this->renderSuccess(compact('list'),'我的金币提取记录');
    }
}
