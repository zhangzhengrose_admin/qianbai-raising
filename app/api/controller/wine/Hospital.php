<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\Hospital as HospitalModel;

class Hospital extends Controller{
    // 查询所属医院列表
    public function getList(){
        $hospital_id_other = input('hospital_id_other','','string');
        if($hospital_id_other!=''){
            $idStr = str_replace(['[',']'],'',$hospital_id_other);
        }else{
            $idStr = '';
        }
        $list = HospitalModel::getList($idStr);
        return $this->renderSuccess(compact('list'),'获取项目所属医院列表成功');
    }
}
