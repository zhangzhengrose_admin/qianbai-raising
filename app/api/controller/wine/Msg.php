<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use think\App;

class Msg extends Controller{

    private $user_id;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }
    // 获取消息列表
    public function getList(){
        $class_msg = input('class_msg',0,'int');
        $list = \app\common\model\Msg::getList($this->user_id,$class_msg);
        return $this->renderSuccess(compact('list'),'消息列表获取成功');
    }
}
