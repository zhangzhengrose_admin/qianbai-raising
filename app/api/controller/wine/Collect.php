<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use \app\api\model\wine\Collect as CollectModel;
use app\api\validate\wine\Collect as CollectValidate;
use think\App;

class Collect extends Controller{
    private $user_id;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 我的收藏
    public function getList(){
        $list = CollectModel::getList($this->user_id);
        return $this->renderSuccess(compact('list'),'我的收藏获取成功');
    }

    // 添加收藏
    public function collected(){
        $params = $this->postForm();
        // 验证参数
        $CollectValidate = new CollectValidate();
        if(!$CollectValidate->scene('collect')->check($params)){
            return $this->renderError($CollectValidate->getError());
        }
        $params['user_id'] = $this->user_id;
        if(!CollectModel::collected($params)){
            return $this->renderError('添加收藏失败');
        }
        return $this->renderSuccess('收藏添加成功');
    }

    // 取消收藏
    public function unfavorite(){
        $params = $this->postForm();
        // 验证参数
        $CollectValidate = new CollectValidate();
        if(!$CollectValidate->scene('collect')->check($params)){
            return $this->renderError($CollectValidate->getError());
        }
        $params['user_id'] = $this->user_id;
        if(!CollectModel::unfavorite($params)){
            return $this->renderError('取消收藏失败');
        }
        return $this->renderSuccess('取消收藏成功');
    }
}
