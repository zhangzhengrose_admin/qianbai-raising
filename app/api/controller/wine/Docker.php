<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\SeeDocker;
use think\App;

class Docker extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 申请找医药
    public function addDocker(){
        $params = $this->postForm();
        $validate = new \app\api\validate\wine\Docker();
        if(!$validate->scene('add')->check($params)){
            return $this->renderError($validate->getError());
        }
        $params['user_id'] = $this->user_id;
        if(! SeeDocker::add($params)){
            return $this->renderError('申请找医药失败');
        }
        return $this->renderSuccess('申请找医药成功');
    }
}
