<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;

class Illness extends Controller{
    // 获取疾病类型列表
    public function getList(){
        $list = \app\api\model\wine\Illness::getList();
        return $this->renderSuccess(compact('list'),'获取疾病类型列表成功');
    }
}
