<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use think\App;

class Patient extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 添加患病人群信息
    public function addPatient(){
        $params = $this->postForm();
        $validate = new \app\api\validate\wine\Patient();
        if(!$validate->scene('add')->check($params)){
            return $this->renderError($validate->getError());
        }
        $params['user_id'] = $this->user_id;
        $patient_id = \app\api\model\wine\Patient::add($params);
        if(empty($patient_id)){
            return $this->renderError('添加患病人群信息失败');
        }
        $pid = $params['pid'] ?? 0;
        \app\api\service\wine\ProjectPart::partProject( (int)$params['project_id'],$this->user_id,(int)$pid,(int)$patient_id);
        // 用户成功报名送积分
        \app\api\service\wine\Dermember::addPointsAction($this->user_id);
        return $this->renderSuccess('添加患病人群信息成功');
    }
}
