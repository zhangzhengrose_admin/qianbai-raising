<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\Dermember as DermemberModel;
use think\App;

class Dermember extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 我的金币榜单
    public function goldList(){
        $user_id = $this->user_id;
        $list = DermemberModel::goldList();
        return $this->renderSuccess(compact('list'),'金币榜单获取成功!');
    }
}
