<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\Derrecord as DerrecordModel;
use think\App;

class Derrecord extends Controller{
    private $userArr;
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        // 验证登陆
        $this->userArr = $this->getLoginUser();
        $this->user_id = $this->userArr['user_id'];
    }
    // 根据级别查询经销商会员列表
    public function getDear(){
        $params = $this->postForm();
        $DerrecordValidate = new \app\api\validate\wine\Derrecord();
        if(!$DerrecordValidate->check($params)){
            return $this->renderError($DerrecordValidate->getError());
        }
        $level = $params['level'];
        $list = DerrecordModel::findDear($this->user_id,$level);
        return $this->renderSuccess(compact('list'),'查询成功');
    }
}
