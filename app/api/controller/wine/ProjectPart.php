<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use think\App;

class ProjectPart extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 用户参与项目
    /*public function participate(){
        $params = $this->postForm();
        $projectPartValidate = new \app\api\validate\wine\ProjectPart();
        if(!$projectPartValidate->scene('participate')->check($params)){
            return $this->renderError($projectPartValidate->getError());
        }
        $pid = $params['pid'] ?? 0;
        \app\api\service\wine\ProjectPart::partProject( (int)$params['project_id'],$this->user_id,(int)$pid);
        // 用户成功报名送积分
        \app\api\service\wine\Dermember::addPointsAction($this->user_id);
        return $this->renderSuccess('项目报名成功');
    }*/

    // 我的推荐（项目）
    public function mylist(){
        $status = input('status',0,'int');
        $list = \app\api\model\wine\ProjectPart::myProject($this->user_id,$status);
        return $this->renderSuccess(compact('list'),'我的推荐项目列表获取成功!');
    }

    // 我的推荐人数统计
    public function statistics(){
        $data = \app\api\model\wine\ProjectPart::statistics($this->user_id);
        return $this->renderSuccess(compact('data'),'推荐人数统计获取成功!');
    }

    // 我的报名项目
    public function myProjectList(){
        $status = input('status',0,'int');
        $list = \app\api\model\wine\ProjectPart::myPartInList($this->user_id,$status);
        return $this->renderSuccess(compact('list'),'我的报名项目列表获取成功!');
    }
}
