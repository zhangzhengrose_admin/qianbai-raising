<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use think\App;

/**
 * 经销商推荐金币记录
 */
class Degain extends Controller{
    private $userArr = [];
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        // 验证登陆
        $this->userArr = $this->getLoginUser();
        $this->user_id = $this->userArr['user_id'];
    }

    /**
     * 获取用户金币记录
     * @return array|\think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function getGoldList(){
        $list = \app\api\model\wine\Degain::getGoldList($this->user_id);
        return $this->renderSuccess(compact('list'),'成功获取用户金币记录!');
    }
}
