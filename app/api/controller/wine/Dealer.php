<?php
namespace app\api\controller\wine;


use app\api\controller\Controller;
use app\api\model\wine\Dermember as DermemberModel;
use app\common\enum\file\FileType as FileTypeEnum;
use app\common\library\storage\Driver as StorageDriver;
use app\store\model\UploadFile as UploadFileModel;
use app\store\model\Setting as SettingModel;
use app\common\enum\Setting as SettingEnum;
use think\App;

class Dealer extends Controller {
    //获取经销商信息
    public function getder(){
        $userArr = $this->getLoginUser();
        $user_id = $userArr['user_id'];
        $info = DermemberModel::getDerInfo($user_id);
        if(empty($info)){
            return $this->renderError('经销商信息获取失败');
        }
        return $this->renderSuccess(compact('info'),'经销商信息获取成功');
    }

    // 获取下线会员数排名
    public function pCountList(){
        $list = DermemberModel::pCountList();
        return $this->renderSuccess(compact('list'),'下线会员数排名获取成功');
    }

    /**
     * 图片上传接口
     * @param int $groupId 分组ID
     * @return array
     * @throws \think\Exception
     */
    public function image(int $groupId = 0)
    {
        $config = SettingModel::getItem(SettingEnum::STORAGE);
        // 实例化存储驱动
        $storage = new StorageDriver($config);
        // 设置上传文件的信息
        $storage->setUploadFile('iFile')
            ->setRootDirName(getStoreId())
            ->setValidationScene('image')
            ->upload();
        // 执行文件上传
        if (!$storage->upload()) {
            return $this->renderError('图片上传失败：' . $storage->getError());
        }
        // 文件信息
        $fileInfo = $storage->getSaveFileInfo();
        // 添加文件库记录
        $model = new UploadFileModel;
        $model->add($fileInfo, FileTypeEnum::IMAGE, $groupId);
        // 图片上传成功
        return $this->renderSuccess(['fileInfo' => $model->toArray()], '图片上传成功');
    }
}
