<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\service\wine\ApplyGrade as ApplyGradeService;
use think\App;

class Applygrade extends Controller{
    private $userArr;
    private $user_id;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->userArr = $this->getLoginUser();
        $this->user_id = $this->userArr['user_id'];
    }

    /**
     * 会员级别申请
     * @return array|\think\response\Json
     * @throws \app\common\exception\BaseException
     */
    public function apply(){
        $grade_id = input('grade_id',0,'int');
        if(empty($grade_id)){
            throwError('grade_id参数缺失',500);
        }
        $checkStatus = ApplyGradeService::check($this->user_id, $grade_id);
        if($checkStatus){
            return $this->renderSuccess('会员级别申请成功');
        }else{
            return $this->renderError('不具备会员级别资格,申请失败');
        }
    }
}
