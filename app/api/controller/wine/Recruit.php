<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use think\App;

class Recruit extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 添加招募信息
    public function addRecruit(){
        $params = $this->postForm();
        $validate = new \app\api\validate\wine\Recruit();
        if(!$validate->scene('add')->check($params)){
            return $this->renderError($validate->getError());
        }
        $params['user_id'] = $this->user_id;
        if(! \app\api\model\wine\Recruit::add($params)){
            return $this->renderError('添加招募信息失败');
        }
        return $this->renderSuccess('添加招募信息成功');
    }
}
