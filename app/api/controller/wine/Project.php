<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\Project as ProjectModel;

class Project extends Controller{
    // 项目列表
    public function getList(){
        $params = $this->request->param();
        $list = ProjectModel::getList($params);
        return $this->renderSuccess(compact('list'),'获取项目列表成功');
    }

    // 项目详情
    public function detail(){
        $project_id = input('project_id',0,'int');
        if($project_id == 0){
            return $this->renderError('访问非法!项目详情获取失败');
        }
        $detail = ProjectModel::detail($project_id);
        return $this->renderSuccess(compact('detail'),'项目详情获取成功');
    }
}
