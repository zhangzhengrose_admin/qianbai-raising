<?php
namespace app\api\controller\wine;

use app\api\controller\Controller;
use app\api\model\wine\Customer as CustomerModel;
use app\api\validate\wine\Customer as CustomerValidate;
use think\App;

class Customer extends Controller{
    private $user_id;
    public function __construct(App $app)
    {
        parent::__construct($app);
        $userArr = $this->getLoginUser();
        $this->user_id = $userArr['user_id'];
    }

    // 添加健康人群信息
    public function addHealthy(){
        $params = $this->postForm();
        $CustomerValidate = new CustomerValidate();
        if(!$CustomerValidate->scene('add')->check($params)){
            return $this->renderError($CustomerValidate->getError());
        }
        $params['user_id'] = $this->user_id;
        $customerId = CustomerModel::add($params);
        if(empty($customerId)){
            return $this->renderError('添加健康人群信息失败');
        }
        $pid = $params['pid'] ?? 0;
        \app\api\service\wine\ProjectPart::partProject( (int)$params['project_id'],$this->user_id,(int)$pid,(int) $customerId);
        // 用户成功报名送积分
        \app\api\service\wine\Dermember::addPointsAction($this->user_id);
        return $this->renderSuccess('添加健康人群信息成功');
    }
}
