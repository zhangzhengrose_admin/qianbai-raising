<?php
namespace app\api\controller;

use app\api\model\UserExt as UserextModel;
use app\api\model\wine\Dermember;
use app\api\validate\authen\Autenticate as ValidateAuthenticate;
use app\common\service\Authenticate as ServiceAuthenticate;
use think\App;

class Authenticate extends Controller{

    //用户信息
    protected $userArr = [];
    protected $userExtModel;
    protected $derModel;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->userArr = $this->getLoginUser();
        $this->userExtModel = new UserextModel();
        $this->derModel = new Dermember();
    }

    //实名认证-银行卡四要素
    public function authen(){
        $params = $this->postForm();
        $validate = new ValidateAuthenticate();
        if(!$validate->scene('authenticate')->check($params)){
            return $this->renderError($validate->getError());
        }
        //判断是否完成实名认证
        $user_id = $this->userArr['user_id'];
        if($this->userExtModel->checkAuthenticate($user_id,$params['idCard'])){
            return $this->renderSuccess([],'你已经完成实名认证');
        }

        $authen = ServiceAuthenticate::sendAuthenticate('authenticate4',[
            'accountNo' => $params['accountNo'],
            'idCard'=> $params['idCard'],
            'name'=> $params['realName'],
            'mobile' => $params['mobile'],
        ]);
        if($authen->getCode()){
            $data = $authen->getResponse();
            if($data->status == '01'){
                $this->userExtModel->saveAuthenticate($user_id,$params['idCard'],$params['realName'],$params['client_ip'],$data);
                $authenticate = $data;
                return $this->renderSuccess(compact('authenticate'));
            }else{
                return $this->renderError($data->msg);
            }
        }
        return $this->renderError();
    }
    //获取实名认证状态
    public function getAuthenticate(){
        $user_id = $this->userArr['user_id'];
        $authens = $this->userExtModel->where(['user_id'=>$user_id])->value('authens');
        if($authens == 1){
            $status = '01';
            $msg = '实名认证通过！';
        }else{
            $status = '02';
            $msg = '实名认证不通过！';
        }
        return $this->renderSuccess(['status'=>$status,'authenticate' => []],$msg);
    }
}
