<?php
namespace app\api\validate\wine;

use think\Validate;

class Derrecord extends Validate{
    protected $rule =   [
        'level'  => 'require|egt:0|elt:3',
    ];

    protected $message  =   [
        'level.require' => '经销商级别必须',
        'level.egt' => '经销商级别必须大于或等于0',
        'level.elt' => '经销商级别必须小于或等于3'
    ];
    protected $scene = [
        'level'  =>  ['level']
    ];
}
