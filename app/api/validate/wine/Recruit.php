<?php
namespace app\api\validate\wine;

use think\Validate;

class Recruit extends Validate{
    protected $rule =   [
        'name'  => 'require',
        'mobile'   => 'require',
        'remark'   => 'require'
    ];

    protected $message  =   [
        'name.require' => '缺参数name',
        'mobile.require'     => '缺参数mobile',
        'remark.require'     => '缺参数remark',
    ];
    protected $scene = [
        'add'  =>  ['name','mobile','remark']
    ];
}
