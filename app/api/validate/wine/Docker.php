<?php
namespace app\api\validate\wine;

use think\Validate;

class Docker extends Validate{
    protected $rule =   [
        'name'  => 'require',
        'mobile'   => 'require',
        'remark'   => 'require',
        'province' => 'require',
        'city' => 'require',
        'area' => 'require',
        'type_illness' => 'require'
    ];

    protected $message  =   [
        'name.require' => '缺参数name',
        'mobile.require'     => '缺参数mobile',
        'remark.require'     => '缺参数remark',
        'province.require'     => '缺参数province',
        'city.require'     => '缺参数city',
        'area.require'     => '缺参数area',
        'type_illness.require'     => '缺参数type_illness',
    ];
    protected $scene = [
        'add'  =>  ['name','mobile','remark','province','city','area','type_illness']
    ];
}
