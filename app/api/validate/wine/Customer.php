<?php
namespace app\api\validate\wine;

use think\Validate;

class Customer extends Validate{
    protected $rule =   [
        'name'  => 'require',
        'mobile'   => 'require',
        'idCard' => 'require',
        'birthday' => 'require',
        'sex' =>'require',
        'age' => 'require',
        'height' => 'require',
        'weight' => 'require',
        'bmi' => 'require',
        'province' => 'require',
        'city' => 'require',
        'area' => 'require',

        'project_id' => 'require'
    ];

    protected $message  =   [
        'name.require'  => '缺少参数name',
        'mobile.require'   => '缺少参数mobile',
        'idCard.require' => '缺少参数idCard',
        'birthday.require' => '缺少参数birthday',
        'sex.require' =>'缺少参数sex',
        'age.require' => '缺少参数age',
        'height.require' => '缺少参数height',
        'weight.require' => '缺少参数weight',
        'bmi.require' => '缺少参数bmi',
        'province.require' => '缺少参数province',
        'city.require' => '缺少参数city',
        'area.require' => '缺少参数area',

        'project_id.require' => '缺少参数project_id'
    ];
    protected $scene = [
        'add'  =>  ['name','mobile','idCard','birthday','sex','age','height','weight','bmi','province','city','area','project_id']
    ];
}
