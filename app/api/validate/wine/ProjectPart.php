<?php
namespace app\api\validate\wine;

use think\Validate;

class ProjectPart extends Validate{
    protected $rule =   [
        'project_id'  => 'require'
    ];

    protected $message  =   [
        'project_id.require' => '缺参数project_id'
    ];
    protected $scene = [
        'participate'  =>  ['project_id']
    ];
}
