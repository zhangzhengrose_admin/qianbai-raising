<?php
namespace app\api\validate\wine;

use think\Validate;

class Collect extends Validate{
    protected $rule =   [
        'project_id' => 'require'
    ];

    protected $message  =   [
        'project_id.require' => '缺少参数project_id'
    ];
    protected $scene = [
        'collect'  =>  ['project_id']
    ];
}
