<?php
namespace app\api\validate\wine;

use think\Validate;

class Dealer extends Validate{
    protected $rule =   [
        'pid'  => 'require',
        'idCard'   => 'require'
    ];

    protected $message  =   [
        'pid.require' => '经销商推荐者id必须',
        'idCard.require'     => '身份证号码必须',
    ];
    protected $scene = [
        'add'  =>  ['pid','idCard']
    ];
}
