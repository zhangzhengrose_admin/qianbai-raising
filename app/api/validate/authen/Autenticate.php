<?php
namespace app\api\validate\authen;

use think\Validate;

class Autenticate extends Validate{
    protected $rule =   [
        'realName'  => 'require',
        'idCard'   => 'require',
        'client_ip' => 'require',
        'accountNo' => 'require',
        'mobile' => 'require',
    ];

    protected $message  =   [
        'realName.require' => '真实姓名必须',
        'idCard.require'     => '身份证号码必须',
        'client_ip.require'     => '客户端ip地址必须',
        'accountNo.require' => '银行卡号必须',
        'mobile.require' => '绑卡手机号必须',
    ];
    protected $scene = [
        'authenticate'  =>  ['realName','idCard','accountNo','mobile','client_ip']
    ];
}
