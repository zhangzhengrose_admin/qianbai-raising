<?php
namespace app\store\controller\project;

use app\store\model\project\Customer;
use app\store\model\project\CustomerPatient;
use app\store\model\project\Illness;
use app\common\library\helper;
use app\store\controller\Controller;
use app\store\model\project\Hospital;
use app\store\model\project\Member;
use app\store\model\project\Recruit;
use app\store\model\project\SeeDocker;

class Manage extends Controller{

    private function getPostForm(){
        $jsonStr = $this->request->param('form','',null);
        return helper::jsonDecode($jsonStr);
    }
    // 获取地区医院列表
    public function hospitalList(){
        $params = $this->request->param();
        $list = (new Hospital)->getListArr($params);
        foreach ($list as $item => $value) {
            $list[$item]['address'] = $value['province'].$value['city'].$value['area'];
        }
        return $this->renderSuccess(compact('list'));
    }

    // 地区医院详情
    public function hospitalDetail(){
        $params = $this->request->param();
        $values = Hospital::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 添加地区医院
    public function hospitalAdd(){
        $params = $this->getPostForm();
        if (Hospital::add($params)) {
            return $this->renderSuccess('添加成功');
        }else{
            return $this->renderError('添加失败');
        }
    }

    // 修改地区医院
    public function hospitalEdit(){
        $params = $this->getPostForm();
        if (Hospital::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }

    // 删除地区医院
    public function hospitalDel(){
        $params = $this->getPostForm();
        if (Hospital::del($params)) {
            return $this->renderSuccess('删除成功');
        }else{
            return $this->renderError('删除失败');
        }
    }

    // 疾病类型列表
    public function illnessList(){
        $params = $this->request->param();
        $list = (new Illness)->getListArr($params);
        return $this->renderSuccess(compact('list'));
    }

    // 疾病类型详情
    public function illnessDetail(){
        $params = $this->request->param();
        $values = Illness::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 添加疾病类型
    public function illnessAdd(){
        $params = $this->getPostForm();
        if (Illness::add($params)) {
            return $this->renderSuccess('添加成功');
        }else{
            return $this->renderError('添加失败');
        }
    }

    // 修改疾病类型
    public function illnessEdit(){
        $params = $this->getPostForm();
        if (Illness::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }

    // 删除疾病类型
    public function illnessDel(){
        $params = $this->getPostForm();
        if (Illness::del($params)) {
            return $this->renderSuccess('删除成功');
        }else{
            return $this->renderError('删除失败');
        }
    }

    // 获取地招募信息列表
    public function recruitList(){
        $params = $this->request->param();
        $list = (new Recruit)->getListArr($params);
        foreach ($list as $item => $value) {
            $list[$item]['userInfo'] = [
                'nickName' => $value['nick_name'],
                'avatar_url' => $value['avatar_url'],
            ];
        }
        return $this->renderSuccess(compact('list'));
    }

    // 招募信息
    public function recruitDetail(){
        $params = $this->request->param();
        $values = Recruit::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 修改招募信息
    public function recruitEdit(){
        $params = $this->getPostForm();
        $values = Recruit::edit($params);
        return $this->renderSuccess(compact('values'),'修改招募信息成功');
    }

    // 获取医药信息列表
    public function dockerList(){
        $params = $this->request->param();
        $list = (new SeeDocker)->getListArr($params);
        foreach ($list as $item => $value) {
            $list[$item]['address'] = $value['province'].$value['city'].$value['area'];
            $list[$item]['userInfo'] = [
                'nickName' => $value['nick_name'],
                'avatar_url' => $value['avatar_url'],
            ];
        }
        return $this->renderSuccess(compact('list'));
    }

    // 找医药信息
    public function dockerDetail(){
        $params = $this->request->param();
        $values = SeeDocker::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 修改找医药信息
    public function dockerEdit(){
        $params = $this->getPostForm();
        $values = SeeDocker::edit($params);
        return $this->renderSuccess(compact('values'),'修改成功');
    }

    // 获取健康人群列表
    public function healthList(){
        $params = $this->request->param();
        $list = (new Customer)->getListArr($params);
        foreach ($list as $item => $value) {
            $list[$item]['address'] = $value['province'].$value['city'].$value['area'];
            // birthday
            $list[$item]['birthday'] = date('Y-m-d',$value['birthday']);
        }
        return $this->renderSuccess(compact('list'));
    }

    // 健康人群详情
    public function healthDetail(){
        $params = $this->request->param();
        $values = Customer::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 添加健康人群
    public function healthAdd(){
        $params = $this->getPostForm();
        if (Customer::add($params)) {
            return $this->renderSuccess('添加成功');
        }else{
            return $this->renderError('添加失败');
        }
    }

    // 修改健康人群
    public function healthEdit(){
        $params = $this->getPostForm();
        if (Customer::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }

    // 删除健康人群
    public function healthDel(){
        $params = $this->getPostForm();
        if (Customer::del($params)) {
            return $this->renderSuccess('删除成功');
        }else{
            return $this->renderError('删除失败');
        }
    }

    // 获取患者人群列表
    public function patientList(){
        $params = $this->request->param();
        $list = (new CustomerPatient)->getListArr($params);
        foreach ($list as $item => $value) {
            $list[$item]['address'] = $value['province'].$value['city'].$value['area'];
            // birthday
            $list[$item]['birthday'] = date('Y-m-d',$value['birthday']);
        }
        return $this->renderSuccess(compact('list'));
    }

    // 患者人群详情
    public function patientDetail(){
        $params = $this->request->param();
        $values = CustomerPatient::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 添加患者人群
    public function patientAdd(){
        $params = $this->getPostForm();
        if (CustomerPatient::add($params)) {
            return $this->renderSuccess('添加成功');
        }else{
            return $this->renderError('添加失败');
        }
    }

    // 修改患者人群
    public function patientEdit(){
        $params = $this->getPostForm();
        if (CustomerPatient::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }

    // 删除患者人群
    public function patientDel(){
        $params = $this->getPostForm();
        if (CustomerPatient::del($params)) {
            return $this->renderSuccess('删除成功');
        }else{
            return $this->renderError('删除失败');
        }
    }

    // 获取会员人群列表
    public function memberList(){
        $params = $this->request->param();
        $list = (new Member)->getListArr($params);
        foreach ($list as $item => $value) {
            $list[$item]['address'] = $value['province'].$value['city'].$value['area'];
            // birthday
            $list[$item]['birthday'] = date('Y-m-d',$value['birthday']);
        }
        return $this->renderSuccess(compact('list'));
    }

    // 会员人群详情
    public function memberDetail(){
        $params = $this->request->param();
        $values = Member::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 添加会员人群
    public function memberAdd(){
        $params = $this->getPostForm();
        if (Member::add($params)) {
            return $this->renderSuccess('添加成功');
        }else{
            return $this->renderError('添加失败');
        }
    }

    // 修改会员人群
    public function memberEdit(){
        $params = $this->getPostForm();
        if (Member::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }
}
