<?php
namespace app\store\controller\project;

use app\common\library\helper;
use app\store\controller\Controller;
use app\store\model\project\Project;

// 招募项目
class Recruit extends Controller{
    private function getPostForm(){
        $jsonStr = $this->request->param('form','',null);
        return helper::jsonDecode($jsonStr);
    }

    // 发布
    public function getList(){
        $params = $this->request->param();
        $list = (new Project)->getList($params);
        foreach ($list as $item => $value) {
            $list[$item]['address'] = $value['province'].$value['city'].$value['area'];
        }
        return $this->renderSuccess(compact('list'));
    }

    // 项目详情
    public function detail(){
        $params = $this->request->param();
        $values = Project::detail($params['id']);
        return $this->renderSuccess(compact('values'));
    }

    // 发布
    public function add(){
        $params = $this->getPostForm();
        if (Project::add($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }

    // 修改
    public function edit(){
        $params = $this->getPostForm();
        if (Project::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }

    // 删除
    public function del(){
        $params = $this->getPostForm();
        if (Project::del($params)) {
            return $this->renderSuccess('删除成功');
        }else{
            return $this->renderError('删除失败');
        }
    }
}
