<?php
namespace app\store\controller\project;

use app\common\library\helper;
use app\store\controller\Controller;
use app\store\model\project\ProjectPart;

class Parting extends Controller{

    private function getPostForm(){
        $jsonStr = $this->request->param('form','',null);
        return helper::jsonDecode($jsonStr);
    }

    // 获取报名用户列表
    public function getList(){
        $params = $this->request->param();
        $list = (new ProjectPart)->getList($params);
        return $this->renderSuccess(compact('list'),'报名用户列表获取成功');
    }

    // 报名用户详情
    public function detail(){
        $params = $this->request->param();
        $values = ProjectPart::detail($params);
        return $this->renderSuccess(compact('values'));
    }

    // 修改用户参与进度
    public function edit(){
        $params = $this->getPostForm();
        if (ProjectPart::edit($params)) {
            return $this->renderSuccess('修改成功');
        }else{
            return $this->renderError('修改失败');
        }
    }
}
