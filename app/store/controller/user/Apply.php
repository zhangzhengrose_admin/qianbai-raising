<?php
namespace app\store\controller\user;

use app\store\controller\Controller;
use app\store\model\ApplyGrade as ApplyGradeModel;
use app\store\model\User as UserModel;
use app\store\service\Authenticate;

class Apply extends Controller{
    // 获取会员级别申请列表
    public function getList()
    {
        $model = new ApplyGradeModel();
        $list = $model->getAppleList($this->request->param());
        return $this->renderSuccess(compact('list'));
    }
    // 会员级别通过
    public function audit(){
        $params = $this->request->param();
        $userId = $params['user_id'];
        // 判断实名认证情况
        if(!Authenticate::getAuthenticate($userId)){
            return $this->renderError('未实名认证，或实名认证不通过!');
        }
        // 用户详情
        $model = UserModel::detail($userId);
        if($model->updateGrade($params) && ApplyGradeModel::setStatus($userId)){
            return $this->renderSuccess('操作成功');
        }
        return $this->renderError($model->getError() ?: '操作失败');
    }
}
