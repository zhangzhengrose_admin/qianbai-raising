<?php
namespace app\store\controller\market;

use app\api\model\Wx as WxModel;
use app\common\library\wechat\WxPay;
use app\common\model\UserOauth;
use app\store\controller\Controller;
use app\store\model\DegainCash;

class Apply extends Controller{
    // 获取会员级别申请列表
    public function getList()
    {
        $model = new DegainCash();
        $list = $model->getAppleList($this->request->param());
        return $this->renderSuccess(compact('list'));
    }
    // 提现到微信零钱
    public function audit(){
        $params = $this->request->param();
        $cash_type = $params['cash_type'];
        if($cash_type == 1){
            $wxgConfig = WxModel::getWxappCache(getStoreId());
            $wxPay = new WxPay([
                'app_id' => $wxgConfig['appid'],
                'app_secret' => $wxgConfig['appsecret'],
                'mchid' => $wxgConfig['mch_id'],
                'apikey' => $wxgConfig['partnerkey'],
                'cert_pem' => $wxgConfig['ssl_cer'],
                'key_pem' => $wxgConfig['ssl_key'],
                'store_id' => $wxgConfig['store_id']
            ]);
            $openId = UserOauth::where(['user_id'=>$params['user_id'],'is_delete'=>0,'oauth_type'=>'G-WEIXIN'])
                ->value('oauth_id');
            $orderNo = date('YmdHis').$params['id'];

            $amount = $params['earnings'];
            $wxPay->transfers($orderNo,$openId,$amount,'金币提现');
            return $this->renderSuccess('提现成功!');
        }else{
            return $this->renderError('暂不支持其他平台提现!');
        }
    }
}
