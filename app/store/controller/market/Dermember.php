<?php
namespace app\store\controller\market;

use app\store\controller\Controller;
use app\store\model\store\Dermember as DermemberModel;
use think\App;

//经销商
class Dermember extends Controller{
    private $derModel;
    public function __construct(App $app)
    {
        $this->derModel = new DermemberModel();
        parent::__construct($app);
    }
    //获取列表
    public function list(){
       $list = $this->derModel->getList($this->request->param());
       return $this->renderSuccess(compact('list'));
    }
}
