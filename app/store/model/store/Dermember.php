<?php
namespace app\store\model\store;

use app\common\model\BaseModel;
use app\store\model\User as UserModel;

class Dermember extends BaseModel{
    // 定义表名
    protected $name = 'Dermember';

    // 定义主键
    protected $pk = 'id';

    // 用户关联数据
    public function user(){
        //关联表关联字段和当前表的关系字段
        return $this->hasOne(UserModel::class,'user_id','user_id')
            ->bind(['mobile','nick_name','avatar_id','pay_money','expend_money']);
    }
    //经销商列表
    public function getList($param){
        $oder = 'desc';
        if(isset($param['orderType'])){
            $oder = $param['orderType'];
        }
        if(isset($param['idCard']) && !empty($param['idCard'])){
            $r = self::with('user')
                ->whereLike('idCard',"%{$param['idCard']}%")
                ->order('create_time',$oder)
                ->paginate($param['limit']);
        }else{
            $r = self::with('user')
                ->order('create_time',$oder)
                ->paginate($param['limit']);
        }
        return $r;
    }
    //获取经销商个人达标奖金
    public function getDBAward($performance){
        $performModel = new Perform();
        $arr = $performModel->order('performance desc')->select();
        foreach ($arr as $value){
            if($performance>$value['performance']){
                return $value;
            }
        }
        return $arr[0];
    }
    //获得经销商奖励
    public function getAward($id){
        $r = self::where(['id'=>$id])->with('user')->find();
        $performance = $r['performance'];
        //达标奖励
        $getDBAward = $this->getDBAward($performance);
        return [
            //达标奖励
            'DBAward'=>$getDBAward['performance'],
            //个人销售额
            'expend_money' => $r['expend_money'],
            //团队收益
            'total_expend_money'=>$r['total_expend_money'],
            //团队成员人数
            'pcount'=>$r['pcount']
        ];
    }
}
