<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\store\model\user;

use app\common\model\BaseModel;
use app\common\model\User;

/**
 * 用户金额变动明细模型
 * Class PointsLog
 * @package app\store\model\user
 */
class Degain extends BaseModel
{
    /**
     * 推荐者关联用户信息
     * @return \think\model\relation\HasOne
     */
    public function user()
    {
        /*$module = self::getCalledModule();
        return $this->belongsTo("app\\{$module}\\model\\User");*/
        return $this->hasOne(User::class,'user_id','user_id');
    }

    /**
     * 被推荐者关联用户信息
     * @return \think\model\relation\HasOne
     */
    public function usersid(){
        return $this->hasOne(User::class,'user_id','sid');
    }

    // 收益类型1推荐奖励,2分享奖励,3活动报名推荐奖励,4活动报名出组奖励,5活动报名交通补贴
    public function getGainTypeAttr($value){
        $status = [1=>'推荐奖励',2=>'分享奖励',3=>'活动报名推荐奖励',4=>'活动报名出组奖励',5=>'活动报名交通补贴'];
        return $status[$value];
    }

    /**
     * 获取积分明细列表
     * @param array $param
     * @return \think\Paginator
     */
    public function getList($param = [])
    {
        // 设置查询条件
        $filter = $this->getFilter($param);
        // 获取列表数据
        return $this->with(['user.avatar','usersid.avatar'])
            ->alias('m')
            ->field('m.*')
            ->where($filter)
            ->join('user', '(user.user_id = m.user_id) or (user.user_id = m.sid)')
            ->order(['m.create_time' => 'desc', $this->getPk()])
            ->paginate(15);
    }

    /**
     * 设置查询条件
     * @param array $param
     * @return array
     */
    private function getFilter(array $param): array
    {
        // 设置默认的检索数据
        $params = $this->setQueryDefaultValue($param, [
            'userId' => 0,          // 会员ID
            'search' => '',         // 搜索内容
            'betweenTime' => [],    // 起止时间
        ]);
        // 检索查询条件
        $filter = [];
        // 用户ID
        $params['userId'] > 0 && $filter[] = ['m.user_id', '=', $params['userId']];
        // 搜索内容: 用户昵称
        !empty($params['search']) && $filter[] = ['user.mobile', 'like', "%{$params['search']}%"];
        // 起止时间
        if (!empty($params['betweenTime'])) {
            $times = between_time($params['betweenTime']);
            $filter[] = ['m.create_time', '>=', $times['start_time']];
            $filter[] = ['m.create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }

}
