<?php
namespace app\store\model;

use app\common\model\BaseModel;
use app\common\model\User as UserModel;
use app\common\model\user\Grade;
use app\store\model\store\Dermember;

class DegainCash extends BaseModel{
    protected $autoWriteTimestamp = true;

    /**
     * 获取查询条件
     * @param array $param
     * @return array
     */
    private function getFilter(array $param = [])
    {
        // 默认查询条件
        $params = $this->setQueryDefaultValue($param, [
            'search' => '',     // 微信昵称
            'gender' => -1,     // 用户性别
            'grade_id' => 0,       // 用户等级
        ]);
        // 检索查询条件
        $filter = [];
//        $filter['search'] = [];
        // 微信昵称
        if(!empty($params['search']) ){
            if(is_numeric($params['search'])){
                $user_id = User::where('mobile','like',"%{$params['search']}%")
                    ->value('user_id');
                $filter[] = ['user_id', '=', $user_id];
            }else{
                $user_id = User::where('nick_name','like',"%{$params['search']}%")
                    ->value('user_id');
                $filter[] = ['user_id', '=', $user_id];
            }
        }

        // 用户性别
        $params['gender'] > -1 && $filter[] = ['gender', '=', (int)$params['gender']];
        // 用户等级
        if($params['grade_id']>0){
            $user_id_f = UserModel::where('grade_id','=',$params['grade_id'])->value('user_id');
            $filter[] = ['user_id', '=', $user_id_f];
        }
        // 起止时间
        if (!empty($params['betweenTime'])) {
            $times = between_time($params['betweenTime']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }

    // 关联会员级别
    public function grade(){
        return $this->hasOne(Grade::class,'grade_id','grade_id')
            ->bind(['grade_name'=>'name']);
    }
    // 关联用户表数据
    public function user()
    {
        return $this->hasOne(UserModel::class, 'user_id', 'user_id')
            ->with(['avatar'])
            ->bind([
                'nick_name',
                'expend_money' => 'pay_money',
                'avatar_url',
                'platform',
                'mobile',
                'grade_id'
            ]);

    }
    // 关联经销商数据
    public function dermerber(){
        return $this->hasOne(Dermember::class,'user_id','user_id')
            ->bind(['pcount']);
    }

    // 支取平台类型1微信2支付宝3银行卡
    public function getCashTagAttr($value,$data){
        $cash_type = $data['cash_type'];
        switch ($cash_type){
            case 1:
                return '微信';
            case 2:
                return '支付宝';
            case 3:
                return '银行卡';
            default:
                return '未定义';
        }
    }

    // 获取待申请经销商数据
    public function getAppleList(array $param = []){
        // 检索查询条件
        $filter = $this->getFilter($param);
        return $this->with(['user','dermerber','grade'])
            ->where($filter)
            ->where(['is_delete'=>0])
            ->append(['cash_tag']) // 使自定义字段生效
            ->paginate(15);
    }
}
