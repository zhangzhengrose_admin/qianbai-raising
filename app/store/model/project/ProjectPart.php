<?php
namespace app\store\model\project;

use app\common\model\BaseModel;
use app\common\model\User;
use app\store\service\project\Part;

class ProjectPart extends BaseModel{
    protected $autoWriteTimestamp = true;

    /**
     * 设置查询条件
     * @param array $param
     * @return array
     */
    private function getFilter(array $param): array
    {
        // 设置默认的检索数据
        $params = $this->setQueryDefaultValue($param, [
            'userId' => 0,          // 会员ID
            'search' => '',         // 搜索内容
            'betweenTime' => [],    // 起止时间
        ]);
        // 检索查询条件
        $filter = [];
        // 搜索内容: 用户昵称
        if(!empty($params['search'])){
            $user_id = User::where(['mobile'=>$params['search']])->value('user_id');
            $filter[] = ['user_id', '=', $user_id];
        }
        // 起止时间
        if (!empty($params['betweenTime'])) {
            $times = between_time($params['betweenTime']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        // 疾病类型
        if (!empty($params['illness_id'])) {
            $project_id_r = Project::where(['type_illness'=>$params['illness_id']])->field('id')->page(1,100)->select();
            $project_arr = [];
            foreach ($project_id_r as $value){
                array_push($project_arr,$value['id']);
            }
            $filter[] = ['project_id', 'in', $project_arr];
        }
        // 参与进度
        if (!empty($params['status'])) {
            $filter[] = ['status', '=', $params['status']];
        }
        return $filter;
    }

    // 项目奖励关联
    public function projectGold()
    {
        return $this->hasOne(Project::class, 'id', 'project_id')
            ->with(['illness','hospital'])
            ->bind(['out_award','gold','transport_gold']);
    }

    // 项目关联表
    public function project()
    {
        return $this->hasOne(Project::class, 'id', 'project_id')
            ->with(['illness','hospital'])
            ->bind(['title','province','city','area','tag','name_h']);
    }

    // 关联用户信息
    public function user(){
        return $this->hasOne(User::class,'user_id','user_id')
            ->with('avatar')
            ->bind(['nick_name','avatar_url','mobile']);
    }

    // 参与进度
    // 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7待完成 8已完成
    public function getStatustagAttr($value,$data){
        $status = $data['status'];
        switch ($status){
            case 1:
                return '待审核';
            case 2:
                return '待签到';
            case 3:
                return '待知情';
            case 4:
                return '待筛选';
            case 5:
                return '待入组';
            case 6:
                return '实验中';
            case 7:
                return '待完成';
            case 8:
                return '已完成';
            default:
                return '未开始';
        }
    }

    // 获取列表数据
    public function getList(array $params)
    {
        $filter = $this->getFilter($params);
        $line = $params['line'] ?? 15;
        return $this->where(['is_delete' => 0])
            ->with(['project','user'])
            ->append(['statustag'])
            ->where($filter)
            ->order('create_time', 'desc')
            ->paginate($line);
    }

    // 详情
    public static function detail(array $params){
        return self::where(['id'=>$params['id'],'is_delete'=>0])
            ->with('project')
            ->find();
    }

    // 修改参与状态
    public static function edit(array $params){
        $data = [];
        // 已审核
        if($params['audit_status'] == 1){
            $data['audit_time'] = time(); // 审核时间
            $data['audit_status'] = 1; // 已审核
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 2;
        }
        // 已签到
        if($params['sign_status'] == 1){
            $data['sign_status'] = 1;
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 3;
        }
        // 已知情
        if($params['informed_status'] == 1){
            $data['informed_status'] = 1;
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 4;
        }
        // 已筛选
        if($params['filter_status'] == 1){
            $data['filter_status'] = 1;
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 5;
        }
        // 已入组
        if($params['group_status'] == 1){
            $data['group_status'] = 1;
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 6;
        }
        // 已实验
        if($params['test_status'] == 1){
            $data['test_status'] = 1;
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 7;
        }
        // 已完成
        if($params['finish_status'] == 1){
            $data['finish_status'] = 1;
            // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7已完成
            $data['status'] = 8;
            // 奖励
            Part::addActionGold($params['id']);
        }
        return self::update($data,['id'=>$params['id']]);
    }
}
