<?php
namespace app\store\model\project;

use app\common\model\BaseModel;

// 健康人群
class Customer extends BaseModel{
    protected $autoWriteTimestamp = true;

    /**
     * 设置查询条件
     * @param array $param
     * @return array
     */
    private function getFilter(array $param): array
    {
        // 设置默认的检索数据
        $params = $this->setQueryDefaultValue($param, [
            'userId' => 0,          // 会员ID
            'search' => '',         // 搜索内容
            'betweenTime' => [],    // 起止时间
        ]);
        // 检索查询条件
        $filter = [];
        // 搜索内容: 用户昵称
        !empty($params['search']) && $filter[] = ['mobile', 'like', "%{$param['search']}%"];
        // 起止时间
        if (!empty($params['betweenTime'])) {
            $times = between_time($params['betweenTime']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }

    // 获取列表数据
    public function getListArr(array $params)
    {
        $line = $params['line'] ?? 15;
        $filter = $this->getFilter($params);
        return $this->where(['is_delete' => 0])
            ->where($filter)
            ->order('create_time', 'desc')
            ->paginate($line);
    }

    // 详情
    public static function detail(int $id){
        $r = self::where(['id'=>$id,'is_delete'=>0])->find();
        $wherein = "{$r['province']},{$r['city']},{$r['area']}";
        $res = (new Region)->whereIn('name',$wherein)->select();
        $r['address'] = [$res[0]['id'],$res[1]['id'],$res[2]['id']];
        $r['birthday'] = date('Y-m-d H:i:s',$r['birthday']);
        return $r;
    }

    // UTC时间格式转UNIX时间戳
    private static function UTCtoUNIX(string $utc){
        $time= str_replace(array('T','Z'),' ',$utc);
        return strtotime($time);
    }

    // // BIM计算公式
    private static function getBIM($height,$weight){
        return $height/$weight^2;
    }

    // 添加内容
    public static function add(array $data){
        $wherein = implode(',',$data['address']);
        $cityR = (new Region)->whereIn('id',$wherein)->select();
        $createData = [
            'store_id' => getStoreId(),
            'user_id' => 0,// 系统
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'idCard' => $data['idCard'],
            'birthday' => self::UTCtoUNIX($data['birthday']),
            'sex' => $data['sex'],
            'age' => $data['age'],
            'height' => $data['height'],
            'weight' => $data['weight'],
            'bmi' => self::getBIM($data['height'],$data['weight']),

            'province' => $cityR[0]['name'],
            'city' => $cityR[1]['name'],
            'area' => $cityR[2]['name']
        ];
        return self::create($createData);
    }

    // 修改
    public static function edit(array $data){
        $wherein = implode(',',$data['address']);
        $cityR = (new Region)->whereIn('id',$wherein)->select();
        return self::update([
            'user_id' => 0, // 系统
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'idCard' => $data['idCard'],
            'birthday' => self::UTCtoUNIX($data['birthday']),
            'sex' => $data['sex'],
            'age' => $data['age'],
            'height' => $data['height'],
            'weight' => $data['weight'],
            'bmi' => self::getBIM($data['height'],$data['weight']),

            'province' => $cityR[0]['name'],
            'city' => $cityR[1]['name'],
            'area' => $cityR[2]['name']
        ],['id'=>$data['id']]);
    }

    // 删除
    public static function del(array $data){
        return self::where(['id'=>$data['id']])->delete();
    }
}
