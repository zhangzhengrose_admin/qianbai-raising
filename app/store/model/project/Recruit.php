<?php
namespace app\store\model\project;

use app\common\model\BaseModel;
use app\common\model\User;

class Recruit extends BaseModel{
    protected $autoWriteTimestamp = true;

    /**
     * 设置查询条件
     * @param array $param
     * @return array
     */
    private function getFilter(array $param): array
    {
        // 设置默认的检索数据
        $params = $this->setQueryDefaultValue($param, [
            'userId' => 0,          // 会员ID
            'search' => '',         // 搜索内容
            'betweenTime' => [],    // 起止时间
        ]);
        // 检索查询条件
        $filter = [];
        // 搜索内容: 用户昵称
        !empty($params['search']) && $filter[] = ['mobile', 'like', "%{$param['search']}%"];
        // 起止时间
        if (!empty($params['betweenTime'])) {
            $times = between_time($params['betweenTime']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }

    // 关联用户信息
    public function user(){
        return $this->hasOne(User::class,'user_id','user_id')
            ->with('avatar')
            ->bind(['nick_name','avatar_url']);
    }

    // 获取列表数据
    public function getListArr(array $params)
    {
        $line = $params['line'] ?? 15;
        $filter = $this->getFilter($params);
        return $this->where(['is_delete' => 0])
            ->where($filter)
            ->with('user')
            ->order('create_time', 'desc')
            ->paginate($line);
    }

    // 详情
    public static function detail(int $id){
        return self::where(['id'=>$id,'is_delete'=>0])->find();
    }

    // 修改
    public static function edit(array $data){
        return self::update([
            'name' => $data['name'],
            'mobile' => $data['mobile'],
            'remark' => $data['remark']
        ],['id'=>$data['id']]);
    }
}
