<?php
namespace app\store\model\project;

use app\common\model\BaseModel;

class Hospital extends BaseModel{
    protected $autoWriteTimestamp = true;

    // 获取列表数据
    public function getListArr(array $params)
    {
        $filter = $this->getFilter($params);
        $line = $params['line'] ?? 15;
        return $this->where(['is_delete' => 0])
            ->where($filter)
            ->order('create_time', 'desc')
            ->paginate($line);
    }

    // 详情
    public static function detail(int $id){
        $r = self::where(['id'=>$id,'is_delete'=>0])->find();
        $wherein = "{$r['province']},{$r['city']},{$r['area']}";
        $res = (new Region)->whereIn('name',$wherein)->select();
        $r['address'] = [$res[0]['id'],$res[1]['id'],$res[2]['id']];
        return $r;
    }

    // 添加内容
    public static function add(array $data){
        $wherein = implode(',',$data['address']);
        $r = (new Region)->whereIn('id',$wherein)->select();
        return self::create([
            'store_id' => getStoreId(),
            'province' => $r[0]['name'],
            'city' => $r[1]['name'],
            'area' => $r[2]['name'],
            'name' => $data['name']
        ]);
    }

    // 修改
    public static function edit(array $data){
        $wherein = implode(',',$data['address']);
        $r = (new Region)->whereIn('id',$wherein)->select();
        return self::update([
            'province' => $r[0]['name'],
            'city' => $r[1]['name'],
            'area' => $r[2]['name'],
            'name' => $data['name']
        ],['id'=>$data['id']]);
    }

    // 删除
    public static function del(array $data){
        return self::where(['id'=>$data['id']])->delete();
    }

    /**
     * 设置查询条件
     * @param array $param
     * @return array
     */
    private function getFilter(array $param): array
    {
        // 设置默认的检索数据
        $params = $this->setQueryDefaultValue($param, [
            'userId' => 0,          // 会员ID
            'search' => '',         // 搜索内容
            'betweenTime' => [],    // 起止时间
        ]);
        // 检索查询条件
        $filter = [];
        // 搜索内容: 用户昵称
        !empty($params['search']) && $filter[] = ['name', 'like', "%{$param['search']}%"];
        // 起止时间
        if (!empty($params['betweenTime'])) {
            $times = between_time($params['betweenTime']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }
}
