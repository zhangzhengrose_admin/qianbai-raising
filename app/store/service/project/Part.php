<?php
namespace app\store\service\project;

use app\api\model\wine\Degain;
use app\common\service\BaseService;
use app\store\model\project\ProjectPart;

class Part extends BaseService{
    /**
     * 添加金币收益记录
     * @param int $id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function addActionGold(int $id):void
    {
        // 收益类型1推荐奖励,2分享奖励,3活动报名推荐奖励,4活动报名出组奖励,5活动报名交通补贴
        $r = ProjectPart::where(['id' => $id])->with('projectGold')->find();
        $pid = $r['pid'];
        $user_id = $r['user_id'];
        $out_award = $r['out_award']; // 出组奖励
        $gold = $r['gold']; // 推荐者奖励金币
        $transport_gold = $r['transport_gold'];// 交通补贴
        $part_type = $r['part_type']; // 用户参与项目方式 0自主参与 1推荐
        if($part_type == 0){
            Degain::AddRecommend($pid, $user_id, $out_award, 4);
        }else if($part_type == 1 && $pid > 0){
            Degain::AddRecommend($pid, $user_id, $gold, 3);
        }
        Degain::AddRecommend($pid, $user_id, $transport_gold, 5);
    }
}
