<?php
namespace app\store\service;

use app\api\model\UserExt as UserextModel;
use app\common\service\BaseService;

class Authenticate extends BaseService{
    //获取实名认证状态
    public static function getAuthenticate(int $user_id): bool
    {
        $authens = UserextModel::where(['user_id' => $user_id])->value('authens');
        return $authens == 1;
    }
}
