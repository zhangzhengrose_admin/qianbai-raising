<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\common\model;

use think\facade\Cache;
use app\common\exception\BaseException;

/**
 * 微信公众号模型
 * Class Wxapp
 * @package app\common\model
 */
class Wx extends BaseModel
{
    // 定义表名
    protected $name = 'wx';

    // 定义主键名
    protected $pk = 'id';

    protected $autoWriteTimestamp = true;

    /**
     * 获取公众号信息
     * @param null $storeId
     * @return array|static|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function detail($storeId = null)
    {
        $r = self::where(['store_id'=>$storeId])->findOrEmpty();
        if($r->isEmpty()){
            return new static();
        }else{
            return $r->find();
        }
    }

    /**
     * 从缓存中获取公众号信息
     * @param null $storeId
     * @return array
     * @throws BaseException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getWxappCache($storeId = null)
    {
        // 公众号id
        is_null($storeId) && $storeId = static::$storeId;
        if (!$data = Cache::get("wx_{$storeId}")) {
            // 获取公众号详情, 解除hidden属性
            $detail = self::detail($storeId);
            empty($detail) && throwError('未找到当前公众号信息');
            // 写入缓存
            $data = $detail->hidden([])->toArray();
            Cache::tag('cache')->set("wx_{$storeId}", $data);
        }
        return $data;
    }

}
