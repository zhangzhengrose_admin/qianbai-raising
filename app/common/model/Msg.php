<?php
namespace app\common\model;

/**
 * 模板消息
 */
class Msg extends BaseModel{
    protected $autoWriteTimestamp = true;

    // 写入普通消息
    public static function setGeneralMsg(string $msg,int $user_id){
        return self::create([
            'store_id' => getStoreId(),
            'user_id' => $user_id,
            'msg' => $msg,
            'class_msg' => 1,
            'class_type' => 1
        ]);
    }

    // 写入积分相关消息
    public static function setIntegralMsg(string $msg,int $user_id){
        return self::create([
            'store_id' => getStoreId(),
            'user_id' => $user_id,
            'msg' => $msg,
            'class_msg' => 2,
            'class_type' => 2
        ]);
    }

    // 写入金币相关消息
    public static function setGoldMsg(string $msg,int $user_id){
        return self::create([
            'store_id' => getStoreId(),
            'user_id' => $user_id,
            'msg' => $msg,
            'class_msg' => 2,
            'class_type' => 3
        ]);
    }

    // 获取消息列表
    public static function getList(int $user_id,int $class_msg){
        $where = ['user_id'=>$user_id];
        if($class_msg!=0){
            $where['class_msg'] = $class_msg;
        }
        return self::where($where)->paginate(15);
    }
}
