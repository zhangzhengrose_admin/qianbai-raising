<?php
namespace app\common\library\authenticate\engine;

class Authenticate{
    public $params;
    private $AppCode,$AppKey,$AppSecret;
    public $error;
    public $code;
    public $msg;
    public $response;
    public $httpCode;

    /**
     * 构造方法
     * Qiniu constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->AppCode = $config['AppCode'];
        $this->AppKey = $config['AppKey'];
        $this->AppSecret = $config['AppSecret'];
    }

    public function auth(){
        $host = "https://idenauthen.market.alicloudapi.com";
        $path = "/idenAuthentication";
        $method = "POST";
        $appcode = $this->AppCode;
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        //根据API的要求，定义相对应的Content-Type
        array_push($headers, "Content-Type".":"."application/x-www-form-urlencoded; charset=UTF-8");
        $params = [
            'idNo' => $this->params['idNo'],
            'name' => $this->params['name'],
        ];
        $bodys = http_build_query($params);
        $url = $host . $path;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
        $this->response = curl_exec($curl);
        //检查是否有错误发生
        if(curl_errno($curl)){
            //发生错误
            $this->code = false;
            $this->httpCode = 500;
            $this->msg = curl_error($curl);
            $this->response = [];
        }else{
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if($httpCode == 200){
                $this->code = true;
                $this->httpCode = $httpCode;
                $this->msg = '请求成功';
                $this->response = json_decode($this->response);
            }else{
                $this->code = false;
                $this->httpCode = $httpCode;
                $this->msg = '请求异常';
                $this->response = [];
            }
        }
        curl_close($curl);
        return $this;
    }

    public function auth4(){
        $params = [
            'accountNo' => $this->params['accountNo'],
            'idCard' => $this->params['idCard'],
            'name' => $this->params['name'],
            'mobile' => $this->params['mobile'],
        ];
        $host = "https://bcard3and4.market.alicloudapi.com";
        $path = "/bankCheck4";
        $method = "GET";
        $appcode = $this->AppCode;//开通服务后 买家中心-查看AppCode
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = http_build_query($params);

        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        if (1 == strpos("$" . $host, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $out_put = curl_exec($curl);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        list($header, $body) = explode("\r\n\r\n", $out_put, 2);
        if ($httpCode == 200) {
            /*print("正常请求计费(其他均不计费)<br>");
            print($body);*/
            $this->code = true;
            $this->httpCode = $httpCode;
            $this->msg = '请求成功';
            $this->response = json_decode($body);
        } else {
            $this->code = false;
            $this->httpCode = $httpCode;
            $this->response = [];
            if ($httpCode == 400 && strpos($header, "Invalid Param Location") !== false) {
                $this->msg = "参数错误";
            } elseif ($httpCode == 400 && strpos($header, "Invalid AppCode") !== false) {
                $this->msg = "AppCode错误";
            } elseif ($httpCode == 400 && strpos($header, "Invalid Url") !== false) {
                $this->msg = "请求的 Method、Path 或者环境错误";
            } elseif ($httpCode == 403 && strpos($header, "Unauthorized") !== false) {
                $this->msg = "服务未被授权（或URL和Path不正确）";
            } elseif ($httpCode == 403 && strpos($header, "Quota Exhausted") !== false) {
                $this->msg = "套餐包次数用完";
            } elseif ($httpCode == 500) {
                $this->msg = "API网关错误";
            } elseif ($httpCode == 0) {
                $this->msg = "URL错误";
            } else {
                $headers = explode("\r\n", $header);
                $headList = array();
                foreach ($headers as $head) {
                    $value = explode(':', $head);
                    $headList[$value[0]] = $value[1];
                }
                print($headList['x-ca-error-message']);
                $this->msg = "参数名错误 或 其他错误";
            }
        }
        return $this;
    }
}
