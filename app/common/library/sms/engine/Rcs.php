<?php
namespace app\common\library\sms\engine;

use app\common\library\sms\package\rcs\RcsSms;

class Rcs extends Server{
    private $config;

    /**
     * 构造方法
     * Qiniu constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * 发送短信通知
     * @param array $sceneConfig 场景配置
     * @param array $templateParams 短信模板参数
     * @return bool
     */
    public function sendSms(array $sceneConfig, array $templateParams){
        // *** 需用户填写部分 ***

        // 必填: 短信接收号码
        $acceptPhone = $sceneConfig['acceptPhone'];

        // 必填: 短信模板id
        $templateCode = $sceneConfig['templateCode'];

        $content = $sceneConfig['content'];


        $helper = new RcsSms($this->config['sid'],$this->config['apikey']);
        //提取content
        $code = $templateParams['code'];
        $arrStr = explode('@',$content);
        $str = $arrStr[1];
        $content = "@{$str}@={$code}";

        $response = $helper -> sendTplSms($templateCode,$content,$acceptPhone);

        $this->error = $response->msg;
        return $response->code == '0';
    }
}
