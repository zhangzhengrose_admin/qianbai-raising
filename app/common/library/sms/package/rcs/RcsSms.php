<?php
namespace app\common\library\sms\package\rcs;

class RcsSms{
    //参数设置
    private $sid      = "ZH000002209"; //短信账号，从会员中心里获取
    private $apikey   = "78030a17-e992-4e91-ab83-3ccddcf7900a";      // APIKEY，从会员中心里获取

// 8030:正式短信接口的服务端口
// 80：测试短信接口的服务端口，如您的短信条数不充足，可以使用测试端口调试接口，测试接口不会下发短信，也不会计费
    private $svr_rest = "http://api.rcscloud.cn:8030/rcsapi/rest";    //rest请求地址

    public function __construct($sid='',$apikey=''){
        $sid != '' && $this->sid = $sid;
        $apikey != '' && $this->apikey = $apikey;
    }


    // *** 1、账号接口 信息获取
    public function queryUser(){
        $sign     = md5($this->sid.$this->apikey); // 签名认证 Md5(sid+apikey)
        $svr_url  =$this->svr_rest ."/user/get.json?sid=". $this->sid."&sign=".$sign; // 服务器接口路径
        return  file_get_contents($svr_url);                // 获取信息
        //  my_print_arr($json_arr,"账号接口：");              // 输出
    }



    // *** 2、查询账号所有模板
    public function queryTpls(){
        $sign     = md5( $this->sid. $this->apikey);  // 签名认证 Md5(sid+apikey)
        $svr_url  = $this->svr_rest."/tpl/gets.json?sid=".$this->sid."&sign=".$sign;// 服务器接口路径
        return  file_get_contents($svr_url);             // 获取信息
    }

    // *** 3、查询单个模板
    public function queryTemplate($tplid){

        $sign     = md5( $this->sid. $this->apikey.$tplid); // 签名认证 Md5(sid+apikey+tplid)
        $svr_url  = $this->svr_rest."/tpl/get.json?sid=".$this->sid."&sign=".$sign."&tplid=".$tplid;    // 服务器接口路径
        return  file_get_contents($svr_url); // 获取信息
    }


// *** 4、模板短信接口  [模板短信没有定时功能]
    public function sendTplSms($tplid,$content, $mobiles, $extno=""){
        //  $tplid    = "69af1cbbb95043478593767935bc6c51";                      // 模板id
        // 参数值，多个参数以“||”隔开 如:@1@=HY001||@2@=3281
        $sign     = md5($this->sid.$this->apikey.$tplid.$mobiles.$content); // 签名认证 Md5(sid+apikey+tplid+mobile+content)
        $svr_url  =$this->svr_rest."/sms/sendtplsms.json";                        // 服务器接口路径

        // POST方式提交服务器
        $post_data = array();
        $post_data["sign"]      = $sign;
        $post_data["sid"]       =  $this->sid;
        $post_data["tplid"]     = $tplid;
        $post_data["mobile"]    = $mobiles;
        $post_data["content"]   = $content;
        $post_data["extno"]=$extno;
        $res=$this->request_post($svr_url, $post_data);
        return   json_decode( $res);
    }


// *** 5、状态接口 信息获取
    public function queryRpt(){
        $sign     = md5($this->sid.$this->apikey);                                          // 签名认证 Md5(sid+apikey)
        $svr_url  = $this->svr_rest."/sms/queryrpt.json?sid=".$this->sid."&sign=".$sign;    // 服务器接口路径
        return  file_get_contents($svr_url);                   // 获取信息
    }


// *** 6、上行接口 信息获取
    public function queryMo(){
        $sign     = md5($this->sid.$this->apikey);                                          // 签名认证 Md5(sid+apikey)
        $svr_url  = $this->svr_rest."/sms/querymo.json?sid=".$this->sid."&sign=".$sign;     // 服务器接口路径
        return file_get_contents($svr_url);                   // 获取信息
        // 输出
    }


// *** 7、检测黑名单
    public function validBL($mobile){
        $sign     = md5($this->sid.$this->apikey);                                       // 签名认证 Md5(sid+apikey)
        $svr_url  = $this->svr_rest."/assist/bl.json?sid=".$this->sid."&sign=".$sign."&mobile=".$mobile;     // 服务器接口路径
        return file_get_contents($svr_url);                   // 获取信息
        // 输出
    }


// *** 8、检测敏感词
    public function validSW($content){
        $content2  = urlencode($content);                                   // 多个敏感词以逗号隔开
        $sign     = md5($this->sid.$this->apikey);                                          // 签名认证 Md5(sid+apikey)
        $svr_url  = $this->svr_rest."/assist/sw.json?sid=".$this->sid."&sign=".$sign."&content=".$content2;     // 服务器接口路径
        return file_get_contents($svr_url);                   // 获取信息
        // 输出
    }



    /**
     * 模拟post进行url请求
     * @param string $url
     * @param array $post_data
     */
    private function request_post($url = '', $post_data = array()) {
        if (empty($url) || empty($post_data)) {
            return false;
        }

        $o = "";
        foreach ( $post_data as $k => $v )
        {
            $o.= "$k=" . urlencode( $v ). "&" ;
        }
        $post_data = substr($o,0,-1);

        $postUrl = $url;
        $curlPost = $post_data;
        $ch = curl_init();//初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl);//抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);//设置header
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Content-Encoding: utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);//要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = curl_exec($ch);//运行curl
        curl_close($ch);
        return $data;
    }
}
