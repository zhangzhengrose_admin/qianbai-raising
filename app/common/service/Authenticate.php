<?php
// +----------------------------------------------------------------------
// | 萤火商城系统 [ 致力于通过产品和服务，帮助商家高效化开拓市场 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 https://www.yiovo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed 这不是一个自由软件，不允许对程序代码以任何形式任何目的的再发行
// +----------------------------------------------------------------------
// | Author: 萤火科技 <admin@yiovo.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace app\common\service;

/**
 * 支付通道服务
 * Class Message
 * @package app\common\service
 */
class Authenticate extends BaseService
{
    /**
     * 场景列表
     * [场景名称] => [场景类]
     * @var array
     */
    private static $sceneList = [
        // 子商户注册接口
        'authenticate' => \app\common\service\authenticate\Authen::class,
        'authenticate4' => \app\common\service\authenticate\Authen4::class,
    ];


    public static function sendAuthenticate(string $sceneName, array $param)
    {
        if (!isset(self::$sceneList[$sceneName])) return false;
        $class = self::$sceneList[$sceneName];
        return (new $class())->exec($param);
    }

}
