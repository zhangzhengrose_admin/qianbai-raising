<?php
namespace app\common\service;

use app\common\model\Msg;

/**
 * 消息通知
 */
class Tempmsg extends BaseService{
    // 普通消息
    public static function setGeneralMsg(string $title,int $user_id){
        return Msg::setGeneralMsg($title,$user_id);
    }

    // 积分相关消息
    public static function setIntegralMsg(float $earnings,int $user_id){
        $msg = "你有新的积分到账，{$earnings}枚";
        return Msg::setIntegralMsg($msg,$user_id);
    }

    // 金币相关消息
    public static function setGoldMsg(float $earnings,int $user_id){
        $msg = "你有新的金币到账，{$earnings}枚";
        return Msg::setGoldMsg($msg,$user_id);
    }
}
