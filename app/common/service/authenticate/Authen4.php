<?php
namespace app\common\service\authenticate;

use app\common\model\store\Setting as SettingModel;
use app\common\service\BaseService;
use app\common\library\authenticate\Driver;

class Authen4 extends BaseService{

    private $params = [];

    public function exec(array $params){
        $this->params = $params;
        return $this->sendAuthen();
    }

    public function sendAuthen(){
        $authenConfig = SettingModel::getItem('authenticate', getStoreId());
        $authen = new Driver($authenConfig);
        $authen->setParams($this->params);
        return $authen->auth4();
    }
}
