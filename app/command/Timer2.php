<?php
declare (strict_types = 1);

namespace app\command;

use app\common\service\Tempmsg;
use app\store\model\project\ProjectPart;
use app\store\model\user\Degain;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Event;
use app\api\model\wine\Dermember;

class Timer2 extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('timer2')
            ->addArgument('start', Argument::REQUIRED, '开始执行任务')
            ->setDescription('the perform command');
    }

    protected function execute(Input $input, Output $output)
    {
        $start = $input->getArgument('start');
        print_r($start);
        switch ($start){
            case 'start':
                // 指令输出
                $output->writeln('StoreTask is starting');
                // 这里执行系统预设的定时任务事件
                Event::trigger('StoreTask');
                break;
            case 'test':
                $output->writeln('测试啊');
                break;
            case 'gold':
                // 金币结算
                $this->gold();
                $output->writeln('金币结算完毕');
                break;
        }

    }

    // 金币结算
    private function gold(){
        // 项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7待完成 8已完成
        $userArr = ProjectPart::where(['status'=>8,'is_delete'=>0])->select();
        foreach ($userArr as $item) {
            $user_id = (int) $item['user_id'];
            // 交通补助 收益类型1推荐奖励,2分享奖励,3活动报名推荐奖励,4活动报名出组奖励,5活动报名交通补贴
            $transportGoldArr = Degain::where(['sid' => $user_id, 'status' => 0])
                ->whereIn('gain_type', [4, 5])
                ->select();
            foreach ($transportGoldArr as $transportVal){
                $id = $transportVal['id'];
                $earnings =(float) $transportVal['earnings'];
                // 增加金币
                Dermember::setGold($user_id,$earnings);
                // 设置已结算
                Degain::where(['id'=>$id])->save(['status'=>1]);
                // 发起消息通知
                Tempmsg::setGoldMsg($earnings,$user_id);
            }
            $DegainArr = Degain::where(['user_id'=>$user_id,'status'=>0])->select();
            foreach ($DegainArr as $val){
                $id = $val['id'];
                $earnings =(float) $val['earnings'];
                // 增加金币
                Dermember::setGold($user_id,$earnings);
                // 设置已结算
                Degain::where(['id'=>$id])->save(['status'=>1]);
                // 发起消息通知
                Tempmsg::setGoldMsg($earnings,$user_id);
            }
        }
    }
}
