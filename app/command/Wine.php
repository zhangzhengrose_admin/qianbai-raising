<?php
declare (strict_types = 1);

namespace app\command;

use app\store\model\store\Dermember;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use app\store\model\User as UserModel;

class Wine extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('wine')
            ->addArgument('start', Argument::REQUIRED, '开始执行任务')
            ->setDescription('the perform command');
    }

    protected function execute(Input $input, Output $output)
    {
        $start = $input->getArgument('start');
        print_r($start);
        switch ($start){
            case 'start':
                // 指令输出
                $output->writeln('dermember表团队销售统计-定时任务开始执行');
                $derModel = new Dermember();
                $jxsArr = $derModel->where(['is_delete'=>0])->with('user')->select();
                foreach ($jxsArr as $value){
                    $id = $value['id'];
                    $getPerform = $this->getPerform($id);
                    $expend_money = $value['pay_money'];
                    $total = $getPerform + $expend_money;
                    $derModel->where(['id'=>$id])->save(['total_expend_money'=>$total]);
                }
                $output->writeln('dermember表团队销售统计-定时任务执行结束');
                break;
            case 'test':
                $output->writeln('测试啊');
                break;
        }

    }
    //查找子经销商业绩
    private function getPerform($pid){
        $derModel = new Dermember();
        $r = $derModel->where(['pid'=>$pid])->with('user')->select();
        $total_expend_money = 0;
        foreach ($r as $value){
            $expend_money = $value['pay_money'];
            $total_expend_money += $expend_money;
        }
        //经销商个数
        $pcount = count($r);
        //统计团队收益
        $derModel->where(['id'=>$pid])->save([
            'total_expend_money'=>$total_expend_money,
            'pcount'=>$pcount
        ]);
        return $total_expend_money;
    }
}
