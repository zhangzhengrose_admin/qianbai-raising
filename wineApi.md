<!-- toc -->
#### 仟佰募接口目录
- [微信公众号索取登陆牌照](#A-微信公众号索取登陆牌照)
- [发送手机号码短信验证](#发送手机号码短信验证)
- [微信公众号注册](#微信公众号注册)
- [微信公众号注册无短信验证码](#微信公众号注册无短信验证码)
- [微信公众号快捷登陆](#微信公众号快捷登陆)
- [绑定手机号](#绑定手机号)
- [我的下线会员](#我的下线会员)
- [经销商排行榜](#经销商排行榜)
- [金币榜单](#金币榜单)
- [收入明细](#收入明细)
- [可提现金币](#可提现金币)
- [金币提现申请](#金币提现申请)
- [金币提取记录](#金币提取记录)
- [我的经销商个人信息](#我的经销商个人信息)
- [获取微信codeURL](#获取微信codeURL)
- [微信公众号推荐链接](#微信公众号推荐链接)
- [判断是否关注微信公众号](#判断是否关注微信公众号)
- [会员级别申请](#会员级别申请)
- [获取所有地区(树状)列表](#获取所有地区)
- [添加收货地址](#添加收货地址)
- [添加健康人群信息](#健康人群报名)
- [添加患病人群信息](#患病人群报名)
- [个人参与信息详情](#个人参与信息详情)
- [修改个人参与信息](#修改个人参与信息)
- [添加招募信息](#添加招募信息)
- [申请找医药](#申请找医药)
- [首页](#首页)
- [获取项目列表](#获取项目列表)
- [获取项目详情](#获取项目详情)
- [我的推荐项目](#我的推荐项目)
- [我的推荐人数统计](#我的推荐人数统计)
- [我报名的项目](#我报名的项目)
- [上传图片](#上传图片)
- [所属医院列表](#所属医院列表)
- [疾病类型列表](#疾病类型列表)
- [实名认证](#实名认证)
- [获取实名认证状态](#获取实名认证状态)
- [文章分类列表](#文章分类列表)
- [文章列表](#文章列表)
- [文章详情](#文章详情)
- [获取当前用户默认收货地址](#获取当前用户默认收货地址)
- [当前收货地址列表](#当前收货地址列表)
- [获取所有地区(树状)](#获取所有地区(树状))
- [添加收货地址](#添加收货地址)
- [设置默认收货地址](#设置默认收货地址)
- [删除收货地址](#删除收货地址)
- [编辑收货地址](#编辑收货地址)
- [收货地址详情](#收货地址详情)
- [我的订单列表](#我的订单列表)
- [我的订单详情](#我的订单详情)
- [立即收货](#立即收货)
- [商品列表](#商品列表)
- [获取商品详情](#获取商品详情)
- [检查订单立即购买模式](#检查订单立即购买模式)
- [立即购买](#立即购买)
- [获取用户信息](#获取用户信息)
- [获取用户消息通知](#获取用户消息通知)
- [我的收藏](#我的收藏)
- [添加收藏](#添加收藏)
- [取消收藏](#取消收藏)
<!-- tocstop -->

## 接口文档

### 微信公众号索取登陆牌照
- des 微信公众号登陆前调取服务器登陆牌照
- header 
    - content-type **application/json**
- method  **GET**
- url     `/api/passport/captcha`
- params
    - 无
- result [json]
    - int status 200
    - string message success
    - object data
        - string base64 图形验证码数据
        - string key    票据，登陆需要用到
        - string md5    校验值
        
### 发送手机号码短信验证
- des 通过短信、图形验证码验证手机号码
- header
    - content-type **application/json**
- method  **POST**
- url     `/api/passport/sendSmsCaptcha` 
- params [json]
    - object form
        - string captchaCode 输入图形验证码
        - string captchaKey  A接口票据即key值(每次需要重新获取)
        - string mobile      手机号码
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - array data json数组，空值为[]

### 微信公众号注册
- des 注册模式下，短信和验证码必传
- header
    - content-type **application/json**
    - platform G-WEIXIN 固定值
- method  **POST**
- url     `/api/passport/login`
- params [json]
    - object form
        - bool isParty true
        - string mobile 手机号（必传）
        - string smsCode 短信验证码 (选传)
        - string pwd 密码 (选传,不传密码系统自动设定默认密码)
        - object partyData {
                   - string code  微信公众号code,请自行搜索微信公众号网页授权code获取
                   - string oauth G-WEIXIN 固定值
                 } 
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[]

### 微信公众号注册无短信验证码
- des 注册模式下，无短信验证码
- header
  - content-type **application/json**
  - platform G-WEIXIN 固定值
- method  **POST**
- url     `/api/passport/loginNSms`
- params [json]
  - object form
    - bool isParty true
    - string mobile 手机号（必传）
    - string pwd 密码 (选传,不传密码系统自动设定默认密码)
    - object partyData {
      - string code  微信公众号code,请自行搜索微信公众号网页授权code获取
      - string oauth G-WEIXIN 固定值
      }
- result [json]
  - int status 200成功 500/404失败
  - string message 消息提示
  - array data json数组，空值为[]

### 微信公众号快捷登陆
- des 完成上述注册流程之后，微信公众号可以快捷登陆
- header
    - content-type **application/json**
    - Access-Token string token
    - platform G-WEIXIN 固定值
- method  **POST**
- url     `/api/passport/gWxLogin`
- params [json]
    - object form
        - string code 微信公众号code,请自行搜索微信公众号网页授权code获取
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[]   


```json
{
    "status": 200,
    "message": "登录成功",
    "data": {
        "userId": 1,
        "token": "cde2481fb286e4a26f978869f41974b6"
    }
}
```

### 绑定手机号
- des 快捷登陆失败后有bindId值时调用绑定手机号接口
- header
  - content-type **application/json**
  - platform G-WEIXIN 固定值
- method  **POST**
- url     `/api/passport/bindMobile`
- params [json]
  - object form
    - bool isParty true
    - string bindId 快捷登陆失败时返回的值
    - string mobile 手机号
    - string smsCode 短信验证码 
    - string pwd 密码 (选传,不传密码系统自动设定默认密码)
    - object partyData {
             - string oauth G-WEIXIN 固定值
             }
- result [json]
  - int status 200成功 500/404失败
  - string message 消息提示
  - array data json数组，空值为[]

### 我的下线会员
- des 我的下线会员（经销商）
- header
    - content-type **application/json**
    - Access-Token string token
    - platform G-WEIXIN 固定值
- method  **POST**
- url     `/api/wine.Derrecord/getDear`
- url?params  [key-var]
    - int page 1 默认是第一页
- params [json]
    - object form
        - int level 下线会员级别1-3级，级别0时查询所有下线会员
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[] 

```json
{
    "status": 200,
    "message": "查询成功",
    "data": {
        "list": {
            "total": 1,
            "per_page": 15,
            "current_page": 1,
            "last_page": 1,
            "data": [
                {
                    "id": 1,
                    "store_id": 10001,
                    "user_id": 1,
                    "level": 1,
                    "mid": 2,
                    "is_delete": 0,
                    "create_time": "2021-09-22 11:09:50",
                    "update_time": "2021-09-22 11:09:50",
                    "nick_name": "苹果树",
                    "avatar_url": "https://img.zhiwuyipin.com/10001/20210922/53c9696f2a2ed04326f2b24ec5cba093.png",
                    "gender": "男"
                }
            ]
        }
    }
}
```

### 经销商排行榜
- des 我的下线会员（经销商）排行榜
- header
    - content-type 无
    - Access-Token string token
    - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Dealer/pCountList`
- url?params  [key-var]
    - int page 1 默认是第一页
- params [json]
    - 无 
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[] 

### 金币榜单
- des 我的金币榜单
- header
  - content-type 无
  - Access-Token string token
  - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Dermember/goldList`
- url?params  [key-var]
  - int page 1 默认是第一页
- params [json]
  - 无
- result [json]
  - int status 200成功 500/404失败
  - string message 消息提示
  - array data json数组，空值为[]

### 收入明细
- des 我的金币榜单
- header
  - content-type 无
  - Access-Token string token
  - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Degain/getGoldList`
- url?params  [key-var]
  - int page 1 默认是第一页
- params [json]
  - 无
- result [json]
  - int status 200成功 500/404失败
  - string message 消息提示
  - array data json数组，空值为[]


### 可提现金币
- des 我的可提现金币
- header
  - content-type 无
  - Access-Token string token
  - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Cash/goldInfo`
- url?params  [key-var]
  - int page 1 默认是第一页
- params [json]
  - 无
- result [json]
  - int status 200成功 500/404失败
  - string message 消息提示
  - array data json数组，空值为[]

### 金币提现申请
- des 金币提取为现金
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Cash/apply`
- params [json]
  - object form
    - string cash_type 1提现至微信
    - string cash_gold 待提取金币个数的现金
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 金币提取记录
- des 我的金币提取记录
- header
  - content-type 无
  - Access-Token string token
  - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Cash/cashGoldList`
- url?params  [key-var]
  - int page 1 默认是第一页
- params [json]
  - 无
- result [json]
  - int status 200成功 500/404失败
  - string message 消息提示
  - array data json数组，空值为[]

### 我的经销商个人信息
- des 我的经销商个人信息（会员）
- header
    - content-type 无
    - Access-Token string token
    - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Dealer/getder`
- url?params  [key-var]
    - int page 1 默认是第一页
- params [json]
    - 无
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[] 

### 获取微信codeURL
- des 用于分享页获取微信code URL
- header [无]
- method  **GET**
- url     `/api/wine.Wechat/getWxCodeUrl`
- url?params  [key-var]
    - string url 重定向url地址 需要使用urlEncode对链接进行处理
- params [无]
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[] 

### 微信公众号推荐链接
- des 用于分享参数二维码链接
- url     `/share/index.html`
- url?params  [key-var]
     - int pid 推荐者user_id, 无推荐者不传

### 判断是否关注微信公众号
- des 先完成网页授权才能调用该接口判断
- header
    - content-type 无
    - Access-Token string token
    - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Wechat/subscribe`
- url?params  [key-var]
    - int page 1 默认是第一页
- params [json]
    - 无
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[] 
    
### 会员级别申请
- des 登陆用户主动申请对应会员级别，像VIP会员、合伙人等
- header
    - content-type 无
    - Access-Token string token
    - platform G-WEIXIN 固定值
- method  **GET**
- url     `/api/wine.Applygrade/apply`
- url?params  [key-var]
    - int grade_id 1 会员级别id，具体数值详见管理系统 会员管理-会员等级
- params [json]
    - 无
- result [json]
    - int status 200成功 500/404失败
    - string message 消息提示
    - array data json数组，空值为[] 

### 获取所有地区
- des 获取所有地区(树状)列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/region/tree`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象
    

### 健康人群报名
- des 添加健康人群信息
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Customer/addHealthy`
- params [json]
  - object form
    - int    pid        推荐者用户id，默认值0，不传则自己报名，传值则为对方报名
    - int    project_id 项目id
    - string name 姓名
    - string mobile 电话
    - string idCard 身份证号码
    - string birthday 生日，格式为UTC时间 YYYY-MM-DDThh:mm:ssZ
    - string sex 性别 1男2女
    - string age 年龄
    - string height 身高
    - string weight 体重
    - string bmi 体指
    - string province 省份
    - string city 城市
    - string area 城区
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 患病人群报名
- des 添加患病人群信息
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Patient/addPatient`
- params [json]
  - object form
    - int    pid        推荐者用户id，默认值0，不传则自己报名，传值则为对方报名
    - int    project_id 项目id
    - string name 姓名
    - string mobile 电话
    - string idCard 身份证号码
    - string birthday 生日，格式为UTC时间 YYYY-MM-DDThh:mm:ssZ
    - string sex 性别 1男2女
    - string age 年龄
    - string province 省份
    - string city 城市
    - string area 城区
    - string type_illness 疾病类型id,通过疾病类型列表接口选取
    - string case 病例报告图片url,格式 ```jsonArr: [{"uid":"10183","name":"logo360.png","status":"done","url":"https://img.qianbaimu.cn/10001/20211025/63b4364c52c4d6bce9c925b431cd9678.png"},{"uid":"10184","name":"login.png","status":"done","url":"https://img.qianbaimu.cn/10001/20211025/20e4ee1c0db6cb6b5ea65a916fa679f6.png"}]```
    - string remark 备注
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 个人参与信息详情
- des 获取个人信息详情
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Member/detail`
- params [json] 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 修改个人参与信息
- des 修改个人信息模式下，可以一个个字段单独提交
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Member/editMember`
- params [json]
  - object form
    - string name 姓名
    - string mobile 电话
    - string idCard 身份证号码
    - string birthday 生日，格式为UTC时间 YYYY-MM-DDThh:mm:ssZ
    - string sex 性别 1男2女
    - string age 年龄
    - string height 身高
    - string weight 体重
    - string province 省份，例如 湖南省
    - string city 城市，例如 长沙市
    - string area 城区，例如 岳麓区
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 添加招募信息
- des 添加招募信息
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Recruit/addRecruit`
- params [json]
  - object form
    - string name 姓名
    - string mobile 电话
    - string remark 备注
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 申请找医药
- des 申请找医药
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Docker/addDocker`
- params [json]
  - object form
    - string name 姓名
    - string mobile 电话
    - string remark 备注
    - string province 省份
    - string city 城市
    - string area 城区
    - string type_illness 疾病类型
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 首页
- des 获取首页数据，无需token即可访，可能包含商城数据，不包含热门项目数据
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/page/detail&pageId=0`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象,详情如下

### 获取项目列表
- des 显示项目列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/wine.Project/getList`
- params
  - int create_day 按创建日期选择（可选）
  - string province 按省选择（可选）
  - string city 按城市选择（可选）
  - string area 按城区选择（可选）
  - float allowance 按补助大于多少金额选择（可选）
  - int scope_day 按周期(天)选择（可选）
  - int is_heat 是否热门项目（可选）
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 获取项目详情
- des 显示项目详情
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/wine.Project/detail`
- params
  - int project_id 项目id
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 我的推荐项目
- des 我推荐的用户已报名的项目列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.ProjectPart/mylist`
- params
  - int status 项目进行的状态，可选，默认状态值为0,项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7待完成 8已完成
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 我的推荐人数统计
- des 统计总推荐人数、完成人数
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.ProjectPart/statistics`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 我报名的项目
- des 我报名的项目列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.ProjectPart/myProjectList`
- params
  - int status 项目进行的状态，可选，默认状态值为0,项目参与进度 1待审核 2待签到 3待知情 4待筛选 5待入组 6实验中 7待完成 8已完成
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

```json
{
    "status": 200,
    "message": "我的报名项目列表获取成功!",
    "data": {
        "list": {
            "total": 2,
            "per_page": 15,
            "current_page": 1,
            "last_page": 1,
            "data": [
                {
                    "id": 2,
                    "store_id": 10001,
                    "user_id": 8,
                    "pid": 0,
                    "patient_id": 2,
                    "customer_id": 0,
                    "order_no": "2021102552541014",
                    "project_id": 2,
                    "part_type": 0,
                    "status": 1,
                    "is_delete": 0,
                    "create_time": "2021-10-25 15:31:16",
                    "update_time": "2021-10-25 15:31:16",
                    "title": "白癜风",
                    "customer": null,
                  // 患者项目
                    "patient": {
                        "id": 2,
                        "store_id": 10001,
                        "user_id": 0,
                        "name": "张政",
                        "mobile": "18908459468",
                        "idCard": "430528198708130039",
                        "birthday": "1633986734",
                        "sex": 1,
                        "age": 18,
                        "province": "湖南省",
                        "city": "长沙市",
                        "area": "开福区",
                        "type_illness": "3",
                        "case": "[{\"uid\":\"10185\",\"name\":\"O1CN01AOACap26nr9Fk790G_!!2208943417707.jpg_960x960Q50s50.jpg\",\"status\":\"done\",\"url\":\"https://img.qianbaimu.cn/10001/20211025/17fdb6984f42a240a90d384bfe7ca924.jpg\"},{\"uid\":\"10186\",\"name\":\"O1CN01pJGLLx26nr9GYJhHz_!!2208943417707.jpg_2200x2200Q100s50.jpg\",\"status\":\"done\",\"url\":\"https://img.qianbaimu.cn/10001/20211025/572f0860b5ee1388234949666924b5b4.jpg\"}]",
                        "remark": "测试",
                        "is_delete": 0,
                        "create_time": "2021-10-12 13:12:47",
                        "update_time": "2021-10-25 15:41:39"
                    }
                },
                {
                    "id": 1,
                    "store_id": 10001,
                    "user_id": 8,
                    "pid": 0,
                    "patient_id": 0,
                    "customer_id": 3,
                    "order_no": "2021102557515598",
                    "project_id": 1,
                    "part_type": 0,
                    "status": 1,
                    "is_delete": 0,
                    "create_time": "2021-10-25 15:30:33",
                    "update_time": "2021-10-25 15:30:33",
                    "title": "肺活量",
                  // 健康项目
                    "customer": {
                        "id": 3,
                        "store_id": 10001,
                        "user_id": 8,
                        "name": "小亮",
                        "mobile": "18627541469",
                        "idCard": "430528198708130039",
                        "birthday": "1633990715",
                        "sex": 1,
                        "age": 18,
                        "height": 177,
                        "weight": 70,
                        "bmi": 25,
                        "province": "湖南省",
                        "city": "长沙市",
                        "area": "开福区",
                        "is_delete": 0,
                        "create_time": "2021-10-11 17:32:07",
                        "update_time": "2021-10-25 15:30:33"
                    },
                    "patient": null
                }
            ]
        }
    }
}
```

### 上传图片
- des 上传图片,直接返回可访问的图片url地址，每次上传图片url地址都会变化，原url地址仍可访问。需自己保存url地址，服务器不保存url
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Dealer/image`
- formData
  - int groupId 0 分组上传，默认0不分组
  - bin iFile 图片上传流媒体
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

```html
data.fileInfo.external_url 为图片访问url
```

### 所属医院列表
- des 查询所属医院列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/wine.Hospital/getList`
- params
  - string hospital_id_other 所有实验医院id，例如多家医院json字符串格式[1,2,22,3]
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象
    
### 疾病类型列表
- des 获取疾病类型列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/wine.Illness/getList`
- params
  - int page 页码(可选 默认1)
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 实名认证
- des 银行卡四要素实名认证查询
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/Authenticate/authen`
- params
  - object form
    - string realName  真实姓名
    - string idCard    身份证号码
    - string client_ip 客户端ip地址
    - string accountNo 银行卡
    - string mobile    绑卡手机号
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 获取实名认证状态
- des 实名认证状态查询
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/Authenticate/getAuthenticate`
- params [无]
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 文章分类列表
- des 查询文章分类列表
- header
  - content-type **application/json（无）**
  - Access-Token **Access-Token值（无）**
- method  **GET**
- url     `/api/article.Category/list`
- params [无]
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象
    
### 文章列表
- des 查询文章列表
- header
  - content-type **application/json（无）**
  - Access-Token **Access-Token值（无）**
- method  **GET**
- url     `/api/article/list`
- params
  - int categoryId 分类id(可选)
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 文章详情
- des 查询文章详情
- header
  - content-type **application/json（无）**
  - Access-Token **Access-Token值（无）**
- method  **GET**
- url     `/api/article/detail`
- params 
  - int articleId 文章id
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 获取当前用户默认收货地址
- des 获取当前用户默认收货地址
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/defaultId`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象


### 当前收货地址列表
- des 获取当前收货地址列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/list`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 获取所有地区(树状)
- des 获取所有地区(树状)列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/region/tree`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 添加收货地址
- des 添加收货地址数据
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/address/add`
- params [json]
  - object form
    - string detail 地址
    - string name 姓名
    - string phone 电话号码
    - array region
      - object
        - int value     获取所有地区接口获得城市id值
        - string label  获取所有地区接口获得城市名称
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 设置默认收货地址
- des 设置默认收货地址功能
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/setDefault`
- params
  - int addressId 收货地址id
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 删除收货地址
- des 删除收货地址功能
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/remove`
- params
  - int addressId 收货地址id
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 编辑收货地址
- des 编辑收货地址功能
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/edit`
- params
  - int addressId 收货地址id
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 收货地址详情
- des 收货地址详情
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/address/detail`
- params
  - int addressId 收货地址id
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象



### 我的订单列表
- des 获取我的订单列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/list`
- params
  - string dataType all全部 payment待付款 received待发货 deliver待收货 comment待评价
  - int page 1第一页
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象


### 我的订单详情
- des 获取我的订单详情
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/detail`
- params
  - int orderId 订单id
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 立即收货
- des 商家发货完毕，买家可以选择立即收货
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/order/receipt`
- params
  - int orderId 订单id
- result
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 商品列表
- des 获取商品列表数据，无需token即可访问
- header
  - content-type **application/json（可选）**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/goods/list`
- params
  - string sortType all综合排序 sales销量排序 price价格排序
  - bool sortPrice 价格排序 (true高到低 false低到高)
  - int categoryId 分类id，必传，值为1
  - string goodsName 商品名称搜索关键词,支持模糊索引
  - int page 页码，默认1
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 获取商品详情
- des 获取获取商品详情数据，无需token即可访问
- header
  - content-type **application/json**
  - Access-Token **Access-Token值（可选）**
- method  **GET**
- url     `/api/goods/detail`
- params
  - int goodsId 商品id
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 检查订单立即购买模式
- des 先检查完订单后再提交订单
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/checkout/order`
- params
  - string mode buyNow(值固定)
  - int  delivery: 0  配送设置id，默认值为0
  - int  couponId: 0  优惠券id，默认值为0
  - bool  isUsePoints: 0 积分抵扣点数，默认值为0,值1为积分抵扣
  - int  goodsId: 2   商品id
  - int  goodsNum: 1  要购买的商品数量
  - string  goodsSkuId: 1_5 商品sku唯一标识
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 立即购买
- des 提交订单-立即购买
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/checkout/submit`
- params
  - int couponId  优惠券id，默认值为0
    - int delivery  配送设置id，默认值为0
  - int goodsId  商品id
  - int goodsNum  要购买的商品数量
  - string goodsSkuId  商品sku唯一标识，例如1_5
  - bool isUsePoints  积分抵扣点数，值为1
  - string mode  "buyNow"
  - int payType  支付类型 微信支付20  余额支付10，这里填10
  - string remark 备注，默认空值
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象    

### 获取用户信息
- des 获取微信用户注册信息
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/user/info`
- params
  - 无
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - array data json数组，空值为[]
      - object userInfo 用户信息对象

### 获取用户消息通知
- des 金币奖励、积分奖励和普通消息通知
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Msg/getList`
- params
  - int class_msg 0 消息分类 1公布消息 2系统消息 0所有消息
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - array data json数组，空值为[]
      - object userInfo 用户信息对象

### 我的收藏
- des 获取我的收藏列表
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **GET**
- url     `/api/wine.Collect/getList`
- params
  - int page 1 默认为第一页
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示（发送成功，请注意查收）
    - array data json数组，空值为[]
      - object userInfo 用户信息对象

### 添加收藏
- des 添加要收藏的项目
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Collect/collected`
- params [json]
  - object form
    - int project_id 项目id
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象

### 取消收藏
- des 取消已收藏的项目
- header
  - content-type **application/json**
  - Access-Token **Access-Token值**
- method  **POST**
- url     `/api/wine.Collect/unfavorite`
- params [json]
  - object form
    - int project_id 项目id
- result [json]
  - int status 200
  - string message success
  - object data
    - int status 200成功 500/404失败
    - string message 消息提示
    - object data json对象
