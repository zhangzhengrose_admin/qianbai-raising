<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        // 定时任务
        'timer' => \app\console\command\Timer::class,
        // windows系统下定时任务
        'timer2' => \app\command\Timer2::class,
        'wine' => \app\command\Wine::class,

    ],
];
